import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:pendelix/database/database.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import 'dart:convert';import 'package:pendelix/ui/base_widget.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:pendelix/article/article.dart';

dynamic _parseAllArticles(String responseBody) {
  // print(responseBody);
  final parsed = json.decode(responseBody);
  return parsed;
}

class ArticleDeep extends StatefulWidget {
  const ArticleDeep({Key key, this.id}) : super(key: key);

  final dynamic id;

  @override
  _ArticleDeepState createState() => _ArticleDeepState();
}

class _ArticleDeepState extends State<ArticleDeep> with SingleTickerProviderStateMixin {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  MediaQueryData _queryData;
  var _db;
  Future<dynamic> _articles;
  String _co = 'nix';
  String _dir;

  void initState() {
    dynamic settings = GlobalConfiguration().get("main");
    // dynamic down = _downloadConfig(settings);
    _db = new DatabaseHelper();
    _articles = _getAllArticles();
    // _saveArticlesForOffline(_articles);
    // _dbInit();
  }

  Future<dynamic> _getAllArticles() async {
    await _db.getDb();
    dynamic settings = GlobalConfiguration().get("main");
    final response = await http.get(settings['apione'] + '/' + widget.id);
    // print(response);
    if (response.statusCode == 200) {
      return compute(_parseAllArticles, response.body);
    } else {
      return false;
    }
  }

  _getWidth(sizingInformation, appSettings) {
    if (sizingInformation.screenSize.width > appSettings['breakpoint']) {
      return appSettings['breakpoint'];
    } else {
      return sizingInformation.screenSize.width;
    }
  }

  _replaceRoute(context, article) async {
    await new Future.delayed(const Duration(milliseconds: 100));
    dynamic art;
    if(article['countAktionen'] > 0){
      art = article['items']['aktion'][0];
    }
    if(article['countLesermarkt'] > 0){
      art = article['items']['lesermarkt'][0];
    }
      Route route = MaterialPageRoute(
          builder: (context) => Article(article: art));
      Navigator.pushReplacement(context, route);
  }

  @override
  Widget build(BuildContext context) {
    dynamic settings = GlobalConfiguration().get("main");
    dynamic appSettings = GlobalConfiguration().get("app");
    _queryData = MediaQuery.of(context);

    return BaseWidget(builder: (context, sizingInformation) {
      return Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            actions: <Widget>[
              IconButton(
                icon: const Icon(Icons.refresh),
                tooltip: settings['refresh'],
                onPressed: () {
                  setState(() {
                    _articles = Future<dynamic>.value(null);
                  });
                  Future.delayed(const Duration(milliseconds: 100), () {
                    setState(() {
                      _articles = _getAllArticles();
                    });
                  });
                },
              ),
            ],
          ),
          body: FutureBuilder<dynamic>(
              future: _articles,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  dynamic art = snapshot.data;
                  if(art['countAktionen'] == 0 && art['countLesermarkt'] == 0){
                    return Center(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                          Container(
                          height: 200,
                          child: FlareActor(
                            "assets/flare/errorx.flr",
                            alignment: Alignment.center,
                            fit: BoxFit.contain,
                            animation: "action",
                          )),
                      /*
                      Icon(
                        Icons.signal_wifi_off,
                        size: 40,
                      ),
                      */
                      Container(
                        width: 200,
                        child: Text(
                          'Der Artikel ist leider nicht mehr verfügbar',
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ]));
                  }else {
                    _replaceRoute(context, snapshot.data);
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                } else if (snapshot.hasError) {
                  return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                              height: 200,
                              child: FlareActor(
                                "assets/flare/notconnected.flr",
                                alignment: Alignment.center,
                                fit: BoxFit.contain,
                                animation: "init",
                              )),
                          /*
                      Icon(
                        Icons.signal_wifi_off,
                        size: 40,
                      ),
                      */
                          Container(
                            width: 200,
                            child: Text(
                              'Es besteht anscheinend keine Verbindung zum Internet',
                              textAlign: TextAlign.center,
                            ),
                          ),
                          MaterialButton(
                            minWidth: 200,
                            color: Colors.blue,
                            onPressed: () {
                              setState(() {
                                _articles = Future<dynamic>.value(null);
                              });
                              Future.delayed(
                                  const Duration(milliseconds: 100), () {
                                setState(() {
                                  _articles = _getAllArticles();
                                });
                              });
                            },
                            child: Text(settings['wifitryagain'],
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold)),
                          )
                        ],
                      ));
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              }));
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
}
