import 'package:flutter/material.dart';
class SABTI extends StatefulWidget {
  final Widget child;
  const SABTI({
    Key key,
    @required this.child,
  }) : super(key: key);
  @override
  _SABTIState createState() {
    return new _SABTIState();
  }
}
class _SABTIState extends State<SABTI> {

  ScrollPosition _position;
  bool _hasCircle = true;
  Color _co = Color.fromRGBO(0, 0, 0, 0.2);
  @override
  void dispose() {
    _removeListener();
    super.dispose();
  }
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _removeListener();
    _addListener();
  }
  void _addListener() {
    _position = Scrollable.of(context)?.position;
    _position?.addListener(_positionListener);
    _positionListener();
  }
  void _removeListener() {
    _position?.removeListener(_positionListener);
  }
  void _positionListener() {
    final FlexibleSpaceBarSettings settings =
    context.inheritFromWidgetOfExactType(FlexibleSpaceBarSettings);

    // print(settings.currentExtent);
    // print(settings.minExtent);
    // print(_position.pixels);

    // bool visible = settings == null || settings.currentExtent <= settings.minExtent;
    // bool visible = settings == null || _position.pixels >= 100.0;

    if(_position.pixels >= 130 && _hasCircle ){
      setState(() {
        _hasCircle = false;
        _co = Color.fromRGBO(0, 0, 0, 0.0);
      });
    }else if(_position.pixels < 130 && !_hasCircle){
      setState(() {
        _hasCircle = true;
        _co = Color.fromRGBO(0, 0, 0, 0.2);
      });
    }

    // print(_position.pixels);

    /*
    if (_visible != visible) {
      setState(() {
        _co = Color.fromRGBO(0, 0, 0, 0.0);
      });
    }
     */
  }
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(left: 10),
        child: SizedBox(
          height: 10,
          width: 10,
          child: CircleAvatar(
            maxRadius: 10,
            minRadius: 10,
            backgroundColor: _co,
            child: BackButton(
              color: Colors.white,
            ),
          ),
        ));
  }
}