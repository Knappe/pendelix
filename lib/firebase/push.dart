import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:pendelix/helper/service_locator.dart';
import 'package:pendelix/helper/service_navigation.dart';
import 'dart:convert';
import 'dart:io';

class PushNotificationsManager {

  PushNotificationsManager._();

  factory PushNotificationsManager() => _instance;

  static final PushNotificationsManager _instance = PushNotificationsManager._();

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  bool _initialized = false;

  Future<void> init() async {

    if (!_initialized) {
      // For iOS request permission first.

      _firebaseMessaging.configure(
          onMessage: (Map<String, dynamic> message) async {
            print("onMessage: $message");
            // TODO
          },
          onLaunch: (Map<String, dynamic> message) async {
            bool isIos = Platform.isIOS;
            bool isAndroid = Platform.isAndroid;
            // print("onLaunch: $message");
            // print(message['data']['id']);
            // print('-----------------------');
            if(isAndroid) {
              locator<NavigationService>().navigateTo(
                  '/articledeep', message['data']['id']);
            }
            if(isIos){
              locator<NavigationService>().navigateTo(
                  '/articledeep', message['id']);
            }
            // locator<NavigationService>().navigateTo('/articledeep', 100);
            // TODO
          },
          onResume: (Map<String, dynamic> message) async {
            bool isIos = Platform.isIOS;
            bool isAndroid = Platform.isAndroid;
            // print("onLaunch: $message");
            // print(message['data']['id']);
            // print('-----------------------');
            if(isAndroid) {
              locator<NavigationService>().navigateTo(
                  '/articledeep', message['data']['id']);
            }
            if(isIos){
              locator<NavigationService>().navigateTo(
                  '/articledeep', message['id']);
            }
            // locator<NavigationService>().navigateTo('/articledeep', message['data']['id']);
            // locator<NavigationService>().navigateTo('/articledeep', 100);
            // TODO
          }
      );
      // _firebaseMessaging.requestNotificationPermissions();

      // For testing purposes print the Firebase Messaging token
      // String token = await _firebaseMessaging.getToken();
      // print("FirebaseMessaging token: $token");

      _initialized = true;
    }
  }

  Future<void> unsubscribe() async {
    _firebaseMessaging.setAutoInitEnabled(false);
    _firebaseMessaging.deleteInstanceID();
    _firebaseMessaging.unsubscribeFromTopic("default");
  }

  Future<String> subscribe() async {
    _firebaseMessaging.requestNotificationPermissions();
    // _firebaseMessaging.configure();
    String token = await _firebaseMessaging.getToken();
    // print("FirebaseMessaging token: $token");
    _firebaseMessaging.subscribeToTopic("default");
    return token;
  }

  Future<String> getToken() async {
    String token = await _firebaseMessaging.getToken();
    return token;
  }

}