import 'dart:async';
import 'dart:convert';
import 'dart:io' as io;
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:global_configuration/global_configuration.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;

  static Database _db;

  dynamic settings;

  Future<Database> getDb() async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    settings = new GlobalConfiguration().get("database");
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "main.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return theDb;
  }

  void _onCreate(Database db, int version) async {
    // When creating the db, create the table
    print('create');
    await db.execute(
        "CREATE TABLE mydata(id INTEGER PRIMARY KEY, items TEXT)");
    await db.execute(
        "CREATE TABLE articlelist(id INTEGER PRIMARY KEY, lastsync INTEGER, items TEXT)");
    await db.execute(
        "CREATE TABLE reportlist(id INTEGER PRIMARY KEY, date INTEGER, lastsync INTEGER, items TEXT)");
    await db.execute(
        "CREATE TABLE push(id INTEGER PRIMARY KEY, ifpush INTEGER, token TEXT)");
    await db.execute(
        "CREATE TABLE popup(id INTEGER PRIMARY KEY, ifpopup INTEGER)");
    /*await db.execute(
        "CREATE TABLE settings(id INTEGER PRIMARY KEY, items TEXT)");*/
    await db.rawInsert(
        "INSERT INTO push (id, ifpush) VALUES (?,?)",
        [settings['mydataid'], 0]);
    await db.rawInsert(
        "INSERT INTO mydata (id) VALUES (?)",
        [settings['mydataid']]);
    await db.rawInsert(
        "INSERT INTO popup (id, ifpopup) VALUES (?,?)",
        [settings['mydataid'], 0]);
    String sec = (DateTime.now().millisecondsSinceEpoch/1000).toStringAsFixed(0);
    double ms =  double.parse(sec);
    await db.rawInsert(
        "INSERT INTO articlelist (lastsync, items) VALUES (?,?)",
        [ms, settings['articlelistid']]);
    // insertMyData();
  }

  /*
  ** popup
   */

  Future<List> getMyPopUp() async {
    var dbClient = await db;
    List<Map> list =
    await dbClient.rawQuery('SELECT * FROM popup WHERE id = ?', [settings['mydataid']]);
    if (list.length > 0) {
      return list;
    }
    return null;
  }

  Future<int> updateMyPopUp() async {
    var dbClient = await db;
    int res = await dbClient.rawUpdate(
        "UPDATE popup SET ifpopup = 1 WHERE id = ?", [settings['mydataid']]);
    return res;
  }

  /*
  ** push
   */

  Future<List> getMyPush() async {
    var dbClient = await db;
    List<Map> list =
    await dbClient.rawQuery('SELECT * FROM push WHERE id = ?', [settings['mydataid']]);
    if (list.length > 0) {
      return list;
    }
    return null;
  }

  Future<int> deleteMyPush() async {
    var dbClient = await db;
    int res = await dbClient.rawUpdate('UPDATE push SET ifpush = 0, token = NULL WHERE id = ?', [settings['mydataid']]);
    return res;
  }

  Future<int> updateMyPush(String token) async {
    print(token);
    var dbClient = await db;
    int res = await dbClient.rawUpdate(
        "UPDATE push SET ifpush = 1, token = ? WHERE id = ?", [token,settings['mydataid']]);
    return res;
  }

  /*
  ** mydata
   */

  Future<int> insertMyData() async {
    var dbClient = await db;
    int res = await dbClient.rawInsert(
        "INSERT INTO mydata (id) VALUES (?)",
        [settings['mydataid']]);
    return res;
  }

  Future<int> deleteMyData() async {
    var dbClient = await db;
    int res = await dbClient.rawDelete('UPDATE mydata SET items = NULL WHERE id = ?', [settings['mydataid']]);
    return res;
  }

  Future<int> updateMyData(String items) async {
    var dbClient = await db;
    int res = await dbClient.rawInsert(
        "UPDATE mydata SET items = ? WHERE id = ?", [items,settings['mydataid']]);
    return res;
  }

  Future<List> getMyData() async {
    var dbClient = await db;
    List<Map> list =
    await dbClient.rawQuery('SELECT * FROM mydata WHERE id = ?', [settings['mydataid']]);
    // print(list);
    if (list.length > 0) {
      return list;
    }
    return null;
  }

  /*
  ** report list
   */

  Future<List> getReportListOne(String id) async {
    var dbClient = await db;
    List<Map> list =
    await dbClient.rawQuery('SELECT * FROM reportlist WHERE id = ?', [id]);
    // print(list);
    if (list.length > 0) {
      return list;
    }
    return null;
  }

  Future<int> updateReportListOne(String items, String id) async {
    var dbClient = await db;
    int res = await dbClient.rawInsert(
        "UPDATE reportlist SET items = ? WHERE id = ?", [items,id]);
    return res;
  }

  Future<int> deleteReportListOne(String id) async {
    var dbClient = await db;
    int res = await dbClient.rawDelete('DELETE FROM reportlist WHERE id = ?', [id]);
    return res;
  }

  Future<List> getReportListAll() async {
    var dbClient = await db;
    List<Map> list =
    await dbClient.rawQuery('SELECT * FROM reportlist ORDER BY date DESC');
    // print(list);
    if (list.length > 0) {
      return list;
    }
    return null;
  }

  Future<int> deleteReportListAll() async {
    var dbClient = await db;
    int res = await dbClient.rawDelete('DELETE FROM reportlist');
    return res;
  }


  /*
  ** article list
   */

  Future<dynamic> getLastSync() async {
    var dbClient = await db;
    List<Map> la =
    await dbClient.rawQuery('SELECT lastsync FROM articlelist WHERE id = ?', [settings['articlelistid']]);
    return la;
  }

  Future<int> updateArticleList(String items) async {
    var dbClient = await db;
    int res = await dbClient.rawInsert(
        "UPDATE articlelist SET items = ? WHERE id = ?", [items,settings['articlelistid']]);
    return res;
  }

  Future<List> getArticleList() async {
    var dbClient = await db;
    List<Map> list =
    await dbClient.rawQuery('SELECT * FROM articlelist WHERE id = ?', [settings['articlelistid']]);
    // print(list);
    if (list.length > 0) {
      return list;
    }
    return null;
  }

}