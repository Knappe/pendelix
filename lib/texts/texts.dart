import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'dart:async';
import 'package:global_configuration/global_configuration.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:html/dom.dart' as dom;
import 'package:pendelix/ui/base_widget.dart';

Future<String> _loadLocalHTML(name) async {
  return await rootBundle.loadString('assets/texts/' + name + '.html');
}

_launchUrl(url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

_getWidth(sizingInformation, appSettings) {
  if (sizingInformation.screenSize.width > appSettings['breakpoint']) {
    return appSettings['breakpoint'];
  } else {
    return sizingInformation.screenSize.width;
  }
}

// about
class TextAbout extends StatefulWidget {
  @override
  _TextAboutState createState() => _TextAboutState();
}

class _TextAboutState extends State<TextAbout> {
  dynamic settings = GlobalConfiguration().get("drawer");

  MediaQueryData _queryData;

  @override
  Widget build(BuildContext context) {
    dynamic appSettings = GlobalConfiguration().get("app");
    _queryData = MediaQuery.of(context);

    return FutureBuilder<String>(
      future: _loadLocalHTML('about'),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Scaffold(
              appBar: AppBar(
                title: Text(settings['about']),
                leading: IconButton(
                  icon: new Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
              body: BaseWidget(builder: (context, sizingInformation) {
                return SingleChildScrollView(
                    child: Center(
                        child: Container(
                  width: _getWidth(sizingInformation, appSettings),
                  child: Padding(
                    padding: EdgeInsets.only(
                        left: 20, right: 20, top: 20, bottom: 20),
                    child: Html(
                        useRichText: false,
                        data: snapshot.data,
                        customRender: (node, children) {
                          if (node is dom.Element) {
                            switch (node.localName) {
                              case "p":
                                return Padding(
                                    padding:
                                        EdgeInsets.only(top: 10, bottom: 10),
                                    child: Text(
                                      node.text,
                                    ));
                                break;
                              case "h5":
                                return Padding(
                                    padding:
                                        EdgeInsets.only(top: 10, bottom: 10),
                                    child: Text(node.text,
                                        style: Theme.of(context)
                                            .textTheme
                                            .title
                                            .merge(TextStyle(
                                                fontSize: 22.00,
                                                fontWeight: FontWeight.w600))));
                                break;
                            }
                          }
                        },
                        onLinkTap: (url) {
                          switch (url) {
                            case "dataprotection":
                              // Navigator.of(context).pop();
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => TextDataProtect()),
                              );
                              break;
                          }
                        }),
                  ),
                )));
              }));
        } else if (snapshot.hasError) {
          return Scaffold(
            body: Center(
              child: Text("${snapshot.error}"),
            ),
          );
        }
        return Scaffold(
          body: Center(child: CircularProgressIndicator()),
        );
      },
    );
  }
}

// data protection
class TextImprint extends StatefulWidget {
  @override
  _TextImprintState createState() => _TextImprintState();
}

class _TextImprintState extends State<TextImprint> {
  dynamic settings = GlobalConfiguration().get("drawer");

  MediaQueryData _queryData;

  @override
  Widget build(BuildContext context) {
    dynamic appSettings = GlobalConfiguration().get("app");
    _queryData = MediaQuery.of(context);

    return FutureBuilder<String>(
      future: _loadLocalHTML('imprint'),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Scaffold(
              appBar: AppBar(
                title: Text(settings['imprint']),
                leading: IconButton(
                  icon: new Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
              body: BaseWidget(builder: (context, sizingInformation) {
                return SingleChildScrollView(
                    child: Center(
                        child: Container(
                  width: _getWidth(sizingInformation, appSettings),
                  child: Padding(
                    padding: EdgeInsets.only(left: 20, right: 20, top: 20),
                    child: Html(
                        useRichText: true,
                        data: snapshot.data,
                        linkStyle:
                            Theme.of(context).textTheme.body1.merge(TextStyle(
                                  decoration: TextDecoration.underline,
                                )),
                        customTextStyle: (dom.Node node, TextStyle baseStyle) {
                          if (node is dom.Element) {
                            switch (node.localName) {
                              case "h5":
                                return baseStyle.merge(
                                  Theme.of(context).textTheme.title.merge(
                                      TextStyle(
                                          fontSize: 22.00,
                                          fontWeight: FontWeight.w600)),
                                );
                            }
                          }
                          return baseStyle;
                        },
                        onLinkTap: (url) {
                          _launchUrl(url);
                          // open url in a webview
                        }),
                  ),
                )));
              }));
        } else if (snapshot.hasError) {
          return Scaffold(
            body: Center(
              child: Text("${snapshot.error}"),
            ),
          );
        }
        return Scaffold(
          body: Center(child: CircularProgressIndicator()),
        );
      },
    );
  }
}

// data protection
class TextDataProtect extends StatefulWidget {
  @override
  _TextDataProtectState createState() => _TextDataProtectState();
}

class _TextDataProtectState extends State<TextDataProtect> {
  dynamic settings = GlobalConfiguration().get("drawer");

  MediaQueryData _queryData;

  @override
  Widget build(BuildContext context) {
    dynamic appSettings = GlobalConfiguration().get("app");
    _queryData = MediaQuery.of(context);

    return FutureBuilder<String>(
      future: _loadLocalHTML('dataprotection'),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Scaffold(
              appBar: AppBar(
                title: Text(settings['dataprotection']),
                leading: IconButton(
                  icon: new Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
              body: BaseWidget(builder: (context, sizingInformation) {
                return SingleChildScrollView(
                    child: Center(
                        child: Container(
                  width: _getWidth(sizingInformation, appSettings),
                  child: Padding(
                    padding: EdgeInsets.only(
                        left: 20, right: 20, top: 20, bottom: 20),
                    child: Html(
                        data: snapshot.data,
                        onLinkTap: (url) {
                          print(url);
                          // open url in a webview
                        }),
                  ),
                )));
              }));
        } else if (snapshot.hasError) {
          return Scaffold(
            body: Center(
              child: Text("${snapshot.error}"),
            ),
          );
        }
        return Scaffold(
          body: Center(child: CircularProgressIndicator()),
        );
      },
    );
  }
}

// data protection
class TextPolicy extends StatefulWidget {
  @override
  _TextPolicyState createState() => _TextPolicyState();
}

class _TextPolicyState extends State<TextPolicy> {
  dynamic settings = GlobalConfiguration().get("drawer");

  MediaQueryData _queryData;

  @override
  Widget build(BuildContext context) {
    dynamic appSettings = GlobalConfiguration().get("app");
    _queryData = MediaQuery.of(context);

    return FutureBuilder<String>(
      future: _loadLocalHTML('policy'),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Scaffold(
              appBar: AppBar(
                title: Text(settings['policy']),
                leading: IconButton(
                  icon: new Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
              body: BaseWidget(builder: (context, sizingInformation) {
                return SingleChildScrollView(
                    child: Center(
                        child: Container(
                  width: _getWidth(sizingInformation, appSettings),
                  child: Padding(
                    padding: EdgeInsets.only(left: 20, right: 20, top: 20),
                    child: Html(
                        data: snapshot.data,
                        onLinkTap: (url) {
                          print(url);
                          // open url in a webview
                        }),
                  ),
                )));
              }));
        } else if (snapshot.hasError) {
          return Scaffold(
            body: Center(
              child: Text("${snapshot.error}"),
            ),
          );
        }
        return Scaffold(
          body: Center(child: CircularProgressIndicator()),
        );
      },
    );
  }
}

// contact
class TextContact extends StatefulWidget {
  @override
  _TextContactState createState() => _TextContactState();
}

class _TextContactState extends State<TextContact> {
  dynamic settings = GlobalConfiguration().get("drawer");

  MediaQueryData _queryData;

  @override
  Widget build(BuildContext context) {
    dynamic appSettings = GlobalConfiguration().get("app");
    _queryData = MediaQuery.of(context);

    return FutureBuilder<String>(
      future: _loadLocalHTML('contact'),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Scaffold(
              appBar: AppBar(
                title: Text(settings['contact']),
                leading: IconButton(
                  icon: new Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
              body: BaseWidget(builder: (context, sizingInformation) {
                return SingleChildScrollView(
                    child: Center(
                        child: Container(
                  width: _getWidth(sizingInformation, appSettings),
                  child: Padding(
                    padding: EdgeInsets.only(left: 20, right: 20, top: 20),
                    child: Html(
                        useRichText: true,
                        data: snapshot.data,
                        linkStyle:
                            Theme.of(context).textTheme.body1.merge(TextStyle(
                                  decoration: TextDecoration.underline,
                                )),
                        customTextStyle: (dom.Node node, TextStyle baseStyle) {
                          if (node is dom.Element) {
                            switch (node.localName) {
                              case "h5":
                                return baseStyle.merge(
                                  Theme.of(context).textTheme.title.merge(
                                      TextStyle(
                                          fontSize: 22.00,
                                          fontWeight: FontWeight.w600)),
                                );
                            }
                          }
                          return baseStyle;
                        },
                        onLinkTap: (url) {
                          _launchUrl(url);
                          // open url in a webview
                        }),
                  ),
                )));
              }));
        } else if (snapshot.hasError) {
          return Scaffold(
            body: Center(
              child: Text("${snapshot.error}"),
            ),
          );
        }
        return Scaffold(
          body: Center(child: CircularProgressIndicator()),
        );
      },
    );
  }
}

// help
class TextHelp extends StatefulWidget {
  @override
  _TextHelpState createState() => _TextHelpState();
}

class _TextHelpState extends State<TextHelp> {
  dynamic settings = GlobalConfiguration().get("drawer");

  MediaQueryData _queryData;

  @override
  Widget build(BuildContext context) {
    dynamic appSettings = GlobalConfiguration().get("app");
    _queryData = MediaQuery.of(context);

    return FutureBuilder<String>(
      future: _loadLocalHTML('help'),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Scaffold(
              appBar: AppBar(
                title: Text(settings['help']),
                leading: IconButton(
                  icon: new Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
              body: BaseWidget(builder: (context, sizingInformation) {
                return SingleChildScrollView(
                    child: Center(
                        child: Container(
                  width: _getWidth(sizingInformation, appSettings),
                  child: Padding(
                      padding: EdgeInsets.only(
                          left: 20, right: 20, top: 20, bottom: 20),
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(top: 5),
                            ),
                            Text(
                              "So funktioniert die App",
                              style: Theme.of(context).textTheme.title.merge(
                                  TextStyle(
                                      fontSize: 22.00,
                                      fontWeight: FontWeight.w600)),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 20),
                            ),
                            Text(
                              "AZ/AN Service ist die offizielle Feedback-App von Aachener Zeitung und Aachener Nachrichten. Mit dieser App können Sie bequem von Ihrem mobilen Gerät aus eine Reihe von Service-Leistungen und Kontakt-Möglichkeiten rund um die Produkte und Aktionen der beiden Zeitungen und des Medienhauses Aachen nutzen.",
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 20),
                            ),
                            Text(
                                "Die App gliedert sich in zwei Bereiche, die Sie über den unteren Bildschirmrand erreichen können. Zum einen \"Aktionen\", zum anderen \"Leserservice\", wo Sie Service-Angebote und Kontakt-Möglichkeiten  rund um Ihr Abonnement finden."),
                            Padding(
                              padding: EdgeInsets.only(top: 20),
                            ),
                            Text(
                                "Am oberen Bildschirmrand finden Sie neben dem AZ/AN Service-Logo drei Symbole."),
                            Padding(
                              padding: EdgeInsets.only(top: 20),
                            ),
                            RichText(
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text: "Mit dem ",
                                    style: Theme.of(context).textTheme.body2,
                                  ),
                                  WidgetSpan(
                                    child: Icon(Icons.refresh, size: 14),
                                  ),
                                  TextSpan(
                                    text:
                                        " können Sie die Inhalte der App neu laden.",
                                    style: Theme.of(context).textTheme.body2,
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 20),
                            ),
                            RichText(
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text: "Mit dem ",
                                    style: Theme.of(context).textTheme.body2,
                                  ),
                                  WidgetSpan(
                                    child: Icon(Icons.help_outline, size: 14),
                                  ),
                                  TextSpan(
                                    text:
                                        " erreichen Sie die Erklärungsseite, die erläutert, was Sie mit der App alles machen können.",
                                    style: Theme.of(context).textTheme.body2,
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 20),
                            ),
                            RichText(
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text: "Das ",
                                    style: Theme.of(context).textTheme.body2,
                                  ),
                                  WidgetSpan(
                                    child: Icon(Icons.menu, size: 14),
                                  ),
                                  TextSpan(
                                    text:
                                        " führt zu den Einstellungen. Hier finden Sie Informationen rund um die App wie Hilfe, Impressum, Kontaktmöglichkeiten, aber auch Datenschutz-Hinweise und Nutzungsbedingungen.",
                                    style: Theme.of(context).textTheme.body2,
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 20),
                            ),
                            RichText(
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text:
                                        "Besonders hilfreich ist der Punkt Meine Daten ",
                                    style: Theme.of(context).textTheme.body2,
                                  ),
                                  WidgetSpan(
                                    child: Icon(Icons.settings, size: 14),
                                  ),
                                  TextSpan(
                                    text:
                                        ". Hier können Sie einige persönliche Daten (nur lokal auf dem Gerät) abspeichern, die Ihnen dann die Nutzung der App erleichtern. So müssen Sie beispielsweise nicht bei jeder Anfrage oder Reklamation Ihre Daten wie Name, Adresse oder Aboplus-Nummer neu eingeben.",
                                    style: Theme.of(context).textTheme.body2,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )),
                )));
              }));
        } else if (snapshot.hasError) {
          return Scaffold(
            body: Center(
              child: Text("${snapshot.error}"),
            ),
          );
        }
        return Scaffold(
          body: Center(child: CircularProgressIndicator()),
        );
      },
    );
  }
}
