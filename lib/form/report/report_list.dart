import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:pendelix/database/database.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:pendelix/ui/base_widget.dart';
import 'package:pendelix/firebase/push.dart';
import 'package:flutter_custom_dialog/flutter_custom_dialog.dart';
import 'package:pendelix/form/report/report_form.dart';

class ReportList extends StatefulWidget {
  ReportList({Key key}) : super(key: key);

  @override
  _ReportListState createState() => _ReportListState();
}

class _ReportListState extends State<ReportList> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _dateFormat = DateFormat("yyyy-MM-dd");
  ScrollController _scrollController = new ScrollController();
  var _db;

  bool _hasReportList = false;
  String _sent = 'start';
  dynamic _reportList;
  MediaQueryData _queryData;

  void initState() {
    _dbInit();
    _getReportListAll();
  }

  void _dbInit() async {
    _db = new DatabaseHelper();
    await _db.getDb();
  }

  Future _deleteReportListAll() async {
    await _db.deleteReportListAll();
    await _getReportListAll();
    setState(() {
      _hasReportList = false;
      _sent = 'start';
    });
  }

  void _showDeleteDialog(settings) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(settings['reset']['title']),
          content: new Text(
            settings['reset']['text'],
            style: Theme.of(context).textTheme.body1.merge(TextStyle()),
          ),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new RaisedButton(
              child: new Text(
                settings['reset']['yes'].toString().toUpperCase(),
                style: TextStyle(
                    letterSpacing: 1.25,
                    fontFamily: 'Roboto-Bold',
                    fontSize: 12,
                    fontWeight: FontWeight.w500),
              ),
              onPressed: () {
                _formKey.currentState.reset();
                _deleteReportListAll();
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
                _scrollController.animateTo(0.00,
                    duration: Duration(milliseconds: 1), curve: Curves.ease);
              },
            ),
            new RaisedButton(
              child: new Text(
                settings['reset']['cancel'].toString().toUpperCase(),
                style: TextStyle(
                    letterSpacing: 1.25,
                    fontFamily: 'Roboto-Bold',
                    fontSize: 12,
                    fontWeight: FontWeight.w500),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future _getReportListAll() async {
    List reportList;
    reportList = await _db.getReportListAll();
    if (reportList != null) {
      if (reportList[0]['items'] != null) {
        _reportList = json.decode(reportList[0]['items']);
        setState(() {
          // _isPush = _myData['push'];
          _hasReportList = true;
        });
      }
    }
  }

  Widget _btnDeleteReportList(settings) {
    if (_hasReportList) {
      return IconButton(
        icon: Icon(Icons.delete),
        onPressed: () {
          _showDeleteDialog(settings);
        },
      );
    } else {
      return Container();
    }
  }

  _getWidth(sizingInformation, appSettings) {
    if (sizingInformation.screenSize.width > appSettings['breakpoint']) {
      return appSettings['breakpoint'];
    } else {
      return sizingInformation.screenSize.width;
    }
  }


  Widget _getList(settings, appSettings, sizingInformation, queryData) {

    if(!_hasReportList){
      return Center(
        child: Text('Keine Meldungen gespeichert'),
      );
    }else{
      return SingleChildScrollView(
          controller: _scrollController,
          child: Center(
              child: Container(
                  width: _getWidth(sizingInformation, appSettings),
                  child: Padding(
                      padding: EdgeInsets.only(
                          left: 20, right: 20, top: 20, bottom: 20),
                      child: Container()))));
    }

  }

  @override
  Widget build(BuildContext context) {
    bool isDark = MediaQuery.of(context).platformBrightness == Brightness.dark;

    Color actionButtonBack;
    Color actionButtonTest;

    actionButtonBack = Color(0XFF333333);
    actionButtonTest = Colors.white;

    if (isDark) {
      actionButtonBack = Colors.white;
      actionButtonTest = Color(0XFF333333);
    }

    dynamic settings = GlobalConfiguration().get("report");
    dynamic appSettings = GlobalConfiguration().get("app");
    _queryData = MediaQuery.of(context);

    return BaseWidget(builder: (context, sizingInformation) {
      return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          /*
          leading: IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
           */
          title: Text(settings['title']),
          actions: <Widget>[_btnDeleteReportList(settings)],
        ),
        body: _getList(settings, appSettings, sizingInformation, _queryData),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            // Add your onPressed code here!
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ReportForm()),
            );
          },
          backgroundColor: actionButtonBack,
          child: Icon(Icons.add, color: actionButtonTest),
        ),
      );
      // _getForm(settings, appSettings, sizingInformation, _queryData));
    });
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the widget tree.
    // This also removes the _printLatestValue listener.
    super.dispose();
  }
}
