import 'package:flutter/material.dart';
import 'package:pendelix/drawer/drawer.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:pendelix/database/database.dart';
import 'package:http/http.dart';
import 'package:flutter/foundation.dart';
import 'dart:convert';
import 'package:transparent_image/transparent_image.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;
import 'package:url_launcher/url_launcher.dart';

class MessageForm extends StatefulWidget {
  const MessageForm({Key key, this.article, this.type}) : super(key: key);

  final String type;
  final dynamic article;

  @override
  _MessageFormState createState() => _MessageFormState();
}

class _MessageFormState extends State<MessageForm> with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  MediaQueryData _queryData;
  var _db;
  List _buttons;

  void initState() {
    dynamic settings = GlobalConfiguration().get("form");
    // print(_buttons);
    _dbInit();
  }

  void _dbInit() async {
    _db = new DatabaseHelper();
    await _db.getDb();
  }

  Widget _getForm(settings) {
    return SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(left:20, right:20, top:20, bottom: 20),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("jo")
                ],
              )
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    dynamic settings = GlobalConfiguration().get("form");

    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          leading: IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          title: Text(widget.article['titel']),
          actions: <Widget>[

          ],
        ),
        body: _getForm(settings));
  }

  @override
  void dispose() {
    super.dispose();
  }
}
