import 'package:flutter/material.dart';
import 'dart:io' show Platform, File;
import 'dart:async';
import 'package:pendelix/drawer/drawer.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:pendelix/database/database.dart';
import 'package:pendelix/form/message.dart';
import 'package:http/http.dart';
import 'package:flutter/foundation.dart';
import 'dart:convert';
import 'package:transparent_image/transparent_image.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pendelix/form/video.dart';
import 'package:pendelix/ui/base_widget.dart';
import 'package:video_player/video_player.dart';
import 'package:pendelix/form/form_send.dart';
import 'package:geolocator/geolocator.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_custom_dialog/flutter_custom_dialog.dart';
import 'package:pendelix/form/mydata_popup.dart';

class ArticleForm extends StatefulWidget {
  const ArticleForm({Key key, this.article, this.articleForm, this.type})
      : super(key: key);

  final String type;
  final dynamic article;
  final dynamic articleForm;

  @override
  _ArticleFormState createState() => _ArticleFormState();
}

class _ArticleFormState extends State<ArticleForm>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  var _db;
  List _addFields = List();
  List<Widget> _formFields;
  bool _isPush = false;
  bool _isAbo = false;
  bool _isOptin = false;
  bool _isPosition = false;
  String _price = null;
  dynamic _lat = null;
  dynamic _lng = null;
  dynamic _positionLoader = null;
  dynamic _auswahl = Map();
  dynamic _myPush = null;
  Future<dynamic> _myData;
  File _file;
  Color _colorFileRequired = Colors.blue;
  bool _fileValid = true;
  ScrollController _scrollController = new ScrollController();
  VideoPlayerController _videoController;

  String _email;

  TextEditingController emailController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController streetController = TextEditingController();
  TextEditingController zipController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController fonController = TextEditingController();
  TextEditingController handyController = TextEditingController();
  TextEditingController birthdayController = TextEditingController();
  TextEditingController intnumberController = TextEditingController();
  TextEditingController ibanController = TextEditingController();
  TextEditingController bicController = TextEditingController();

  void initState() {
    _formFields = new List<Widget>();
    _addFields.add("hide");
    _addFields.add("hide");
    _dbInit();
  }

  void _dbInit() async {
    _db = new DatabaseHelper();
    await _db.getDb();
    // _myData = _getMyData();
    _getMyPush();
  }

  Future _imagePicker(source) async {
    switch (source) {
      case "camera":
        var file = await ImagePicker.pickImage(source: ImageSource.camera);
        setState(() {
          _file = file;
        });
        break;
      case "gallery":
      default:
        var file = await ImagePicker.pickImage(source: ImageSource.gallery);
        setState(() {
          _file = file;
        });
        break;
    }
  }

  Future<void> _playVideo(File file) async {
    // print("-----------------------------------------");
    // print(file);
    if (file != null && mounted) {
      await _disposeVideoController();
      _videoController = VideoPlayerController.file(file);
      await _videoController.setVolume(1.0);
      await _videoController.initialize();
      await _videoController.play();
      // await _videoController.seekTo(Duration(milliseconds: 0));
      // await _videoController.setLooping(true);
      // await _videoController.play();
      setState(() {
        _file = file;
      });
    }
  }

  Future _videoPicker(source) async {
    switch (source) {
      case "camera":
        final file = await ImagePicker.pickVideo(source: ImageSource.camera);
        await _playVideo(file);
        //setState(() {
        //  _file = file;
        //});
        break;
      case "gallery":
      default:
        final file = await ImagePicker.pickVideo(source: ImageSource.gallery);
        await _playVideo(file);
        // setState(() {
        //  _file = file;
        //});
        break;
    }
  }

  Future<void> _disposeVideoController() async {
    if (_videoController != null) {
      await _videoController.dispose();
      _videoController = null;
    }
  }

  Future<dynamic> _getMyPush() async {
    List pu = await _db.getMyPush();
    if (pu != null) {
      _myPush = pu[0];
      setState(() {
        _myPush = _myPush;
      });
    } else {}
  }

  Future<dynamic> _getMyData(settings) async {
    List myData;
    myData = await _db.getMyData();
    if (myData != null) {
      if (myData[0]['items'] != null) {
        // print(json.decode(myData[0]['fields']));
        return json.decode(myData[0]['items']);
        // print(_myData);
        /*
        emailController.value =
            emailController.value.copyWith(text: _myData['email']);
        nameController.value =
            emailController.value.copyWith(text: _myData['name']);
        streetController.value =
            streetController.value.copyWith(text: _myData['street']);
        zipController.value =
            zipController.value.copyWith(text: _myData['zip']);
        cityController.value =
            cityController.value.copyWith(text: _myData['city']);
        fonController.value =
            fonController.value.copyWith(text: _myData['fon']);
        handyController.value =
            handyController.value.copyWith(text: _myData['mobile']);
        birthdayController.value =
            birthdayController.value.copyWith(text: _myData['birthday']);
        intnumberController.value =
            intnumberController.value.copyWith(text: _myData['intnumber']);
        ibanController.value =
            ibanController.value.copyWith(text: _myData['iban']);
        bicController.value =
            bicController.value.copyWith(text: _myData['bic']);
        */
        return _myData;
      }
    }
    return settings['objdef'];
  }

  Future _sendMyData() async {
    Map<String, dynamic> toJson() => {
          'email': emailController.value.text,
          'name': nameController.value.text,
          'street': streetController.value.text,
          'zip': zipController.value.text,
          'city': cityController.value.text,
          'fon': fonController.value.text,
          'mobile': handyController.value.text,
          'birthday': birthdayController.value.text,
          'intnumber': intnumberController.value.text,
          'iban': ibanController.value.text,
          'bic': bicController.value.text,
          'push': _isPush
        };
    await _db.updateMyData(jsonEncode(toJson()));
  }

  _validators(dynamic required, String errorText) {
    if (required == "1" || required == 1) {
      // print(required);
      // print(errorText);
      return [FormBuilderValidators.required(errorText: errorText)];
    } else {
      return [FormBuilderValidators.min(0)];
    }
  }

  Widget _textFieldAddfield(
      int index,
      String name,
      dynamic required,
      String placeholder,
      String errorText,
      int maxLines,
      double paddingbottom) {
    return Padding(
      padding: EdgeInsets.only(bottom: paddingbottom),
      child: FormBuilderTextField(
        style: Theme.of(context).textTheme.body1.merge(TextStyle(height: 1)),
        attribute: name,
        maxLines: maxLines,
        decoration: InputDecoration(
          labelText: _ifRequiredLabel(placeholder.toUpperCase(), 1),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(0)),
        ),
        onChanged: (value) {
          String addkey = index.toString() + "_add";
          _auswahl[addkey] = value;
        },
        validators: _validators(required, errorText),
      ),
    );
  }

  Widget _textField(
      String name,
      dynamic required,
      String placeholder,
      String errorText,
      int maxLines,
      double paddingbottom,
      TextEditingController controller,
      mydata) {
    return Padding(
      padding: EdgeInsets.only(bottom: paddingbottom),
      child: FormBuilderTextField(
        style: Theme.of(context).textTheme.body1.merge(TextStyle(height: 1)),
        attribute: name,
        keyboardType: TextInputType.text,
        maxLines: maxLines,
        autofocus: false,
        initialValue: mydata,
        /*controller: controller,*/
        decoration: InputDecoration(
            labelText: _ifRequiredLabel(placeholder, required),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(
                    0)) /*,
          border: OutlineInputBorder(
            borderSide: BorderSide(width: 5.0),
          ),*/
            ),
        validators: _validators(required, errorText),
      ),
    );
  }

  double _getSelectPadding(item, addField) {
    if (addField == "show") {
      return 0.0;
    } else {
      return 10.00;
    }
  }

  Text _getSubmitButtonText(settings, te) {
    if (widget.type == "bestellung") {
      return Text(
        settings['ordersend'].toString().toUpperCase(),
        style: TextStyle(color: te, fontSize: 14),
      );
    } else {
      return Text(
        settings['send'].toString().toUpperCase(),
        style: TextStyle(color: te, fontSize: 14),
      );
    }
  }

  _previewFile(sizingInformation) {
    // print(sizingInformation.toString());
    switch (widget.type) {
      case "V":
        if (_file != null) {
          return Padding(
              padding: EdgeInsets.only(bottom: 20),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: sizingInformation.localWidgetSize.width / 2,
                    child: AspectRatioVideo(_videoController),
                  ),
                  // Image.file(_file, width: sizingInformation.localWidgetSize.width/1.5),
                  Positioned(
                      right: 0,
                      top: 0,
                      child: FloatingActionButton(
                        onPressed: () {
                          setState(() {
                            _file = null;
                          });
                        },
                        backgroundColor: Color(0XFF333333),
                        mini: true,
                        child: Icon(Icons.clear, color: Colors.white),
                      ))
                ],
              )
              // child: Image.file(_file, width: sizingInformation.localWidgetSize.width/2),
              );
        } else {
          return Container();
        }
        break;
      case "B":
      default:
        if (_file != null) {
          return Padding(
              padding: EdgeInsets.only(bottom: 20),
              child: Stack(
                children: <Widget>[
                  Image.file(_file,
                      width: sizingInformation.localWidgetSize.width / 2),
                  Positioned(
                      left: 0,
                      top: 0,
                      child: FloatingActionButton(
                        onPressed: () {
                          setState(() {
                            _file = null;
                          });
                        },
                        backgroundColor: Color(0XFF333333),
                        mini: true,
                        child: Icon(Icons.clear, color: Colors.white),
                      ))
                ],
              )
              // child: Image.file(_file, width: sizingInformation.localWidgetSize.width/2),
              );
        } else {
          return Container();
        }
        break;
    }
  }

  _getFileButtons(settings, sizingInformation, switchOn, switchOff) {
    // print(widget.type);
    switch (widget.type) {
      case "V":
        return Padding(
            padding: EdgeInsets.only(top: 20, bottom: 20),
            child: Column(children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[_previewFile(sizingInformation)],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      _videoPicker("gallery");
                    },
                    child: ClipOval(
                      child: Container(
                        color: _colorFileRequired,
                        height: 70.0, // height of the button
                        width: 70.0, // width of the button
                        child: Center(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.video_library,
                              color: switchOff,
                              size: 30,
                            ),
                            Text(settings['video']['btngallery'],
                                style:
                                    TextStyle(fontSize: 10, color: switchOff))
                          ],
                        )),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      _videoPicker("camera");
                    },
                    child: ClipOval(
                      child: Container(
                        color: _colorFileRequired,
                        height: 70.0, // height of the button
                        width: 70.0, // width of the button
                        child: Center(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.videocam,
                              color: switchOff,
                              size: 30,
                            ),
                            Text(settings['video']['btncamera'],
                                style:
                                    TextStyle(fontSize: 10, color: switchOff))
                          ],
                        )),
                      ),
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _fileValid
                      ? Container()
                      : Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: Text(settings['video']['required'],
                              style: TextStyle(
                                  color: Theme.of(context).errorColor,
                                  fontSize: 12)))
                ],
              )
            ]));
        break;
      case "B":
        return Padding(
            padding: EdgeInsets.only(top: 20, bottom: 15),
            child: Column(children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[_previewFile(sizingInformation)],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      _imagePicker("gallery");
                    },
                    child: ClipOval(
                      child: Container(
                        color: _colorFileRequired,
                        height: 70.0, // height of the button
                        width: 70.0, // width of the button
                        child: Center(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.photo_album,
                              color: switchOff,
                              size: 30,
                            ),
                            Text(settings['image']['btngallery'],
                                style:
                                    TextStyle(fontSize: 10, color: switchOff))
                          ],
                        )),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      _imagePicker("camera");
                    },
                    child: ClipOval(
                      child: Container(
                        color: _colorFileRequired,
                        height: 70.0, // height of the button
                        width: 70.0, // width of the button
                        child: Center(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.add_a_photo,
                              color: switchOff,
                              size: 30,
                            ),
                            Text(settings['video']['btncamera'],
                                style:
                                    TextStyle(fontSize: 10, color: switchOff))
                          ],
                        )),
                      ),
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _fileValid
                      ? Container()
                      : Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: Text(settings['image']['required'],
                              style: TextStyle(
                                  color: Theme.of(context).errorColor,
                                  fontSize: 12)))
                ],
              )
            ]));
        break;
    }
  }

  _launchUrl(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _getWidth(sizingInformation, appSettings) {
    if (sizingInformation.screenSize.width > appSettings['breakpoint']) {
      return appSettings['breakpoint'];
    } else {
      return sizingInformation.screenSize.width;
    }
  }

  _ifRequiredLabel(label, required) {
    if (required == 1 || required == "1") {
      return label + " * ".toString().toUpperCase();
    } else {
      return label.toString().toUpperCase();
      ;
    }
  }

  _submitForm() async {

    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ArticleFormSend(
              file: _file,
              data: _fbKey.currentState.fields,
              article: widget.article,
              articleForm: widget.articleForm,
              type: widget.type,
              lat: _lat.toString(),
              lng: _lng.toString(),
              auswahl: _auswahl,
              push: _myPush)),
    );

    /*
    _fbKey.currentState.fields.forEach((key, value) {
      print(key.toString() + ': ' + value.currentState.value);
    });
    */
  }

  _getPosition() async {
    /*
    var ab = YYDialog().build(context)
      ..width = 220
      ..borderRadius = 4.0
      ..animatedFunc = (child, animation) {
        return ScaleTransition(
          child: child,
          scale: Tween(begin: 0.0, end: 1.0).animate(animation),
        );
      }
      ..circularProgress(
        padding: EdgeInsets.only(top: 24.0, bottom: 10),
        valueColor: Colors.blue[500],
      )
      ..text(
        padding: EdgeInsets.only(left: 5.0, bottom: 15.0, right: 5.0),
        alignment: Alignment.center,
        text: "Ihr Standort wird bestimmt...",
        color: Colors.black,
        fontSize: 14.0,
        fontWeight: FontWeight.w500,
      )
      ..show();
     */
    Position position;
    try {
      position = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.low);
    } catch (PlatformException) {
      // print(PlatformException);
      // ab..dismiss();
      // return null;
    }
    return position;
  }

  Widget _responsiveGmapImageLoader(sizingInformation, settings){
    double h = settings['gmap']['imageh'];
    double w = settings['gmap']['imagew'];
    double height = h / w * sizingInformation.localWidgetSize.width;
    return Padding(
        padding: EdgeInsets.only(
          top: 0,
          left: 0,
          right: 0,
          bottom: 10,
        ),
        child: LayoutBuilder(
            builder: (context, constraints) => Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                        width: constraints.maxWidth,
                        height: height,
                        child: Stack(
                          children: <Widget>[
                            Center(
                                child: CircularProgressIndicator()),
                          ],
                        ))
                  ],
                )
              ],
            )));
  }

  Widget _responsiveGMapImage(sizingInformation, settings) {
    double h = settings['gmap']['imageh'];
    double w = settings['gmap']['imagew'];
    double height = h / w * sizingInformation.localWidgetSize.width;
    return Padding(
        padding: EdgeInsets.only(
          top: 0,
          left: 0,
          right: 0,
          bottom: 10,
        ),
        child: LayoutBuilder(
            builder: (context, constraints) => Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                            width: constraints.maxWidth,
                            height: height,
                            child: Stack(
                              children: <Widget>[
                                Center(
                                    child: CachedNetworkImage(
                                  imageUrl: settings['gmap']['staticurl'] +
                                      "?zoom=15&size=" +
                                      sizingInformation.localWidgetSize.width
                                          .toStringAsFixed(0) +
                                      "x" +
                                      height.toStringAsFixed(0) +
                                      "&markers=color:blue%7Clabel:P%7C" +
                                      _lat.toString() +
                                      "," +
                                      _lng.toString() +
                                      "&key=" +
                                      settings['gmap']['apikey'],
                                  placeholder: (context, url) =>
                                      CircularProgressIndicator(
                                    strokeWidth: 1,
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                            Colors.black12),
                                  ),
                                )),
                              ],
                            ))
                      ],
                    )
                  ],
                )));
  }

  Widget _responsiveStaticMapImage(sizingInformation, settings, lat, lng) {
    var apikey = settings['gmap']['apikey'];
    double h = settings['gmap']['imageh'];
    double w = settings['gmap']['imagew'];
    // double height = h / w * queryData.size.width;
    double height = h / w * sizingInformation.localWidgetSize.width;
    return Padding(
        padding: EdgeInsets.only(
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
        ),
        child: LayoutBuilder(
            builder: (context, constraints) => Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          width: constraints.maxWidth,
                          height: height,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: CachedNetworkImageProvider(
                                    settings['gmap']['staticurl'] + apikey)),
                          ),
                        )
                      ],
                    )
                  ],
                )));
  }

  Widget _getFormFields(
      context, settings, appSettings, sizingInformation, mydata) {
    bool isDark = MediaQuery.of(context).platformBrightness == Brightness.dark;

    Color switchBackground;
    Color switchOn;
    Color switchOff;
    Color div;

    switchBackground = Color(0XFF808080);
    switchOn = Color(0XFF333333);
    switchOff = Color(0XFFf1f1f1);
    div = Color(0XFFa6a6a6);
    _colorFileRequired = Color(0XFF333333);

    if (isDark) {
      switchBackground = Color(0XFF808080);
      switchOn = Color(0XFFf1f1f1);
      switchOff = Color(0XFF333333);
      div = Color(0XFF5c5c5c);
      _colorFileRequired = Color(0XFFf1f1f1);
    }

    _formFields = new List<Widget>();

    if (widget.type == "B") {
      _formFields.add(
          _getFileButtons(settings, sizingInformation, switchOn, switchOff));
    }

    if (widget.type == "V") {
      _formFields.add(
          _getFileButtons(settings, sizingInformation, switchOn, switchOff));
    }

    List<dynamic> selectFields = widget.articleForm['arrAuswahl'];
    if (selectFields != null) {
      selectFields.asMap().forEach((index, item) {
        // List<DropdownMenuItem<dynamic>> values = item['wert'];
        List<dynamic> values = item['wert'];
        _formFields.add(Padding(
          padding: EdgeInsets.only(bottom: 10.00, top: 10.00),
          child: FormBuilderDropdown(
            attribute: "auswahl__" + index.toString(),
            decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(0)),
                labelText: _ifRequiredLabel(
                    item['titel'].toString().toUpperCase(), item['required'])),
            // initialValue: 'Male',
            // hint: Text(item['placeholder'], style: TextStyle(letterSpacing: 1)),
            hint: Text(item['placeholder'].toString().toUpperCase(), style: TextStyle(
                letterSpacing: 0.0,
                fontFamily: 'clan-regular',
                fontSize: 14,
                fontWeight: FontWeight.w500)),
            validators:
                _validators(item['required'], settings['select']['required']),
            onChanged: (value) {
              String key = (index + 1).toString();
              _auswahl[key] = item['titel'] +
                  "__" +
                  item['placeholder'] +
                  "__" +
                  value.toString();
              if (item['addfield']['required'] == 1) {
                // print(value);
                String addkey = (index + 1).toString() + "_add";
                if (item['addfield']['parent'] == value) {
                  _addFields[index] = 'show';
                } else {
                  _addFields[index] = 'hide';
                  if (_auswahl[addkey] != null) {
                    _auswahl.remove(addkey);
                  }
                }
                setState(() {
                  // field.didChange(value);
                  _addFields[index] = _addFields[index];
                });
              } else {
                // field.didChange(value);
              }
            },
            items: values
                .map((option) => DropdownMenuItem(
                    value: option,
                    child: Text("$option",
                        style: Theme.of(context)
                            .textTheme
                            .body1
                            .merge(TextStyle(height: 1)))))
                .toList(),
          ),
        ));

        if (item['addfield']['active'] == 1 && _addFields[index] == 'show') {
          _formFields.add(Container(
            height: 15.00,
            width: 15.00,
            child:
                CustomPaint(size: Size(13, 13), painter: DrawTriangle(context)),
          ));

          // Widget _textField(String name, dynamic required, String placeholder, String errorText, int maxLines){
          _formFields.add(_textFieldAddfield(
              index + 1,
              "auswahl__" + index.toString() + "__addfield",
              item['addfield']['required'],
              item['addfield']['placeholder'],
              item['addfield']['error'],
              1,
              20.00));
        }
      });
    } else {
      _formFields.add(Container());
    }

    // meine daten, email ( email ), name ( name ) , street ( strasse ), zip, city, fon, handy, birthday, anzahlpersonen, aboplus

    if (widget.articleForm['arrFeld'] != null) {
      _formFields.add(Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(),
              child: Text(
                settings['mydata']['title'],
              )),
          Padding(
              padding: EdgeInsets.only(),
              child: IconButton(
                  onPressed: () {
                    _showMyDataHelpDialog(settings);
                  },
                  color: Colors.grey,
                  icon: Icon(
                    Icons.help_outline,
                  )))
        ],
      ));
      _formFields.add(Padding(padding: EdgeInsets.only(top: 10)));
      Map fields = widget.articleForm['arrFeld'];
      // email
      if (fields['emailActive'] == "1") {
        String name = settings['email']['var'];
        String requiredString = settings['email']['required'];
        _formFields.add(_textField(
            name,
            fields['emailRequired'],
            settings['email']['placeholder'].toString().toUpperCase(),
            requiredString,
            1,
            10.0,
            emailController,
            mydata['email']));
      }
      if (fields['nameActive'] == "1") {
        String name = settings['name']['var'];
        String requiredString = settings['name']['required'];
        _formFields.add(_textField(
            name,
            fields['nameRequired'],
            settings['name']['placeholder'].toString().toUpperCase(),
            requiredString,
            1,
            10.0,
            nameController,
            mydata['name']));
      }
      if (fields['streetActive'] == "1") {
        String name = settings['street']['var'];
        String requiredString = settings['street']['required'];
        _formFields.add(_textField(
            name,
            fields['streetRequired'],
            settings['street']['placeholder'].toString().toUpperCase(),
            requiredString,
            1,
            10.0,
            streetController,
            mydata['street']));
      }
      if (fields['zipActive'] == "1") {
        String name = settings['zip']['var'];
        String requiredString = settings['zip']['required'];
        _formFields.add(_textField(
            name,
            fields['zipRequired'],
            settings['zip']['placeholder'].toString().toUpperCase(),
            requiredString,
            1,
            10.0,
            zipController,
            mydata['zip']));
      }
      if (fields['cityActive'] == "1") {
        String name = settings['city']['var'];
        String requiredString = settings['city']['required'];
        _formFields.add(_textField(
            name,
            fields['cityRequired'],
            settings['city']['placeholder'].toString().toUpperCase(),
            requiredString,
            1,
            10.0,
            cityController,
            mydata['city']));
      }
      if (fields['fonActive'] == "1") {
        String name = settings['fon']['var'];
        String requiredString = settings['fon']['required'];
        _formFields.add(_textField(
            name,
            fields['fonRequired'],
            settings['fon']['placeholder'].toString().toUpperCase(),
            requiredString,
            1,
            10.0,
            fonController,
            mydata['fon']));
      }
      if (fields['handyActive'] == "1") {
        String name = settings['handy']['var'];
        String requiredString = settings['handy']['required'];
        _formFields.add(_textField(
            name,
            fields['handyRequired'],
            settings['handy']['placeholder'],
            requiredString,
            1,
            10.0,
            handyController,
            mydata['mobile']));
      }
      if (fields['birthdayActive'] == "1") {
        String name = settings['birthday']['var'];
        String requiredString = settings['birthday']['required'];
        _formFields.add(_textField(
            name,
            fields['birthdayRequired'],
            settings['birthday']['placeholder'].toString().toUpperCase(),
            requiredString,
            1,
            10.0,
            birthdayController,
            mydata['birthday']));
      }
      if (fields['anzahlpersonenActive'] == "1") {
        String name = settings['anzahlpersonen']['var'];
        String requiredString = settings['anzahlpersonen']['required'];
        _formFields.add(_textField(
            name,
            fields['anzahlpersonenRequired'],
            settings['anzahlpersonen']['placeholder'].toString().toUpperCase(),
            requiredString,
            1,
            10.0,
            null,
            null));
      }
      if (fields['aboplusActive'] == "1" && widget.type != "O") {
        String name = settings['aboplus']['var'];
        String requiredString = settings['aboplus']['required'];
        _formFields.add(_textField(
            name,
            fields['aboplusRequired'],
            settings['aboplus']['placeholder'].toString().toUpperCase(),
            requiredString,
            1,
            10.0,
            intnumberController,
            mydata['intnumber']));
      }
      if (fields['ibanActive'] == "1") {
        String name = settings['iban']['var'];
        String requiredString = settings['iban']['required'];
        _formFields.add(_textField(
            name,
            fields['ibanRequired'],
            settings['iban']['placeholder'].toString().toUpperCase(),
            requiredString,
            1,
            10.0,
            ibanController,
            mydata['iban']));
      }
      if (fields['bicActive'] == "1") {
        String name = settings['bic']['var'];
        String requiredString = settings['bic']['required'];
        _formFields.add(_textField(
            name,
            fields['bicRequired'],
            settings['bic']['placeholder'].toString().toUpperCase(),
            requiredString,
            1,
            10.0,
            bicController,
            mydata['bic']));
      }
      if (fields['commentActive'] == "1") {
        String name = settings['comment']['var'];
        String requiredString = settings['comment']['required'];
        _formFields.add(_textField(
            name,
            fields['commentRequired'],
            settings['comment']['placeholder'].toString().toUpperCase(),
            requiredString,
            4,
            10.0,
            null,
            null));
      }
    }

    // bestellung
    if (widget.type == "O") {
      if (widget.articleForm['starr'] != null) {
        Map starr = widget.articleForm['starr'];
        Map text = widget.articleForm['text'];
        if (_price == null) {
          _price = text['preisNAbokunde'];
        }
        _formFields.add(Padding(
            padding: EdgeInsets.only(top: 10.00),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(padding: EdgeInsets.only(top: 0.00, bottom: 0.0)),
                  FormBuilderSwitch(
                    activeColor: switchOn,
                    activeTrackColor: switchBackground,
                    inactiveThumbColor: switchOff,
                    // inactiveTrackColor: Colors.grey[800],
                    label: Text(starr['sindSieAbo'],
                        style: Theme.of(context)
                            .textTheme
                            .body1
                            .merge(TextStyle())),
                    attribute: "isAboplus",
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.all(0)),
                    initialValue: false,
                    onChanged: (value) {
                      String _priceN;
                      if (value) {
                        _priceN = text['preisAbokunde'];
                      } else {
                        _priceN = text['preisNAbokunde'];
                      }
                      // print(_priceN);
                      setState(() {
                        _isAbo = value;
                        _price = _priceN;
                      });
                    },
                  )
                ])));
        if (_isAbo) {
          String name = settings['aboplus']['var'];
          String requiredString = settings['aboplus']['required'];
          _formFields.add(Padding(
            padding: EdgeInsets.only(top:15)));
          _formFields.add(_textField(
              name,
              widget.articleForm['aboplusRequired'],
              settings['orderclientid'],
              requiredString,
              1,
              15.0,
              intnumberController,
              mydata['intnumber']));
        }
        _formFields.add(Padding(
            padding: EdgeInsets.only(top: 0.00),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(text['hiermitBestIch'] + " " + _price),
                  Padding(
                    padding: EdgeInsets.only(top: 25),
                  ),
                  Divider(
                    color: div,
                    height: 1,
                    thickness: 1,
                  )
                ])));
      }
    } else {
      _formFields.add(Container());
    }

    if (widget.articleForm['arrFeld']['positionActive'] == "1") {
      _formFields.add(Padding(
              padding: EdgeInsets.only(top: 0.00),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new SwitchListTile(
                      // 333333
                      activeColor: switchOn,
                      activeTrackColor: switchBackground,
                      inactiveThumbColor: switchOff,
                      contentPadding: EdgeInsets.all(0),
                      value: _isPosition,
                      onChanged: (value) {
                        if (value == true) {
                          try {
                            setState(() {
                              _positionLoader = true;
                            });
                            Future pos = _getPosition();
                            pos.then((pos) {
                              if (pos != null) {
                                setState(() {
                                  _positionLoader = null;
                                  _isPosition = true;
                                  _lat = pos.latitude;
                                  _lng = pos.longitude;
                                });
                              } else {
                                setState(() {
                                  _positionLoader = null;
                                  _lat = null;
                                  _lng = null;
                                  _isPosition = false;
                                });
                              }
                            });
                          } catch (e) {
                            print(e);
                          }
                          // print(pos);
                        } else {
                          setState(() {
                            _positionLoader = null;
                            _isPosition = false;
                            _lat = null;
                            _lng = null;
                          });
                        }
                      },
                      title: new Text("Ihre Position übermitteln?",
                          style: Theme.of(context)
                              .textTheme
                              .body1
                              .merge(TextStyle()))),
                ],
              ))

          /*
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(padding: EdgeInsets.only(top: 0.00)),
                FormBuilderSwitch(
                  activeColor: switchOn,
                  activeTrackColor: switchBackground,
                  inactiveThumbColor: switchOff,
                  label: Text("Ihre Position übermitteln?",
                      style:
                          Theme.of(context).textTheme.body1.merge(TextStyle())),
                  attribute: "putPosition",
                  initialValue: _isPosition,
                  onChanged: (value) {
                    if (value == true) {
                      try {
                        Future pos = _getPosition();
                        pos.then((pos) {
                          if(pos != null) {
                            setState(() {
                              _lat = pos.latitude;
                              _lng = pos.longitude;
                            });
                          }else{
                            setState(() {
                              _isPosition = false;
                            });
                          }
                        });
                      }catch(e){

                      }
                      // print(pos);
                    } else {
                      setState(() {
                        _lat = null;
                        _lng = null;
                      });
                    }
                  },
                )
              ]))
          */

          );
    }

    if (_positionLoader != null) {
      _formFields.add(Padding(
          padding: EdgeInsets.only(top: 0.00),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _responsiveGmapImageLoader(sizingInformation, settings)
            ],
          )));
    }

    if (_lat != null) {
      _formFields.add(Padding(
          padding: EdgeInsets.only(top: 0.00),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _responsiveGMapImage(sizingInformation, settings)
            ],
          )));
    }

    if (widget.articleForm['arrFeld']['positionActive'] == "1") {
      _formFields.add(Padding(
          padding: EdgeInsets.only(top: 5.00),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Divider(
                color: div,
                height: 1,
                thickness: 1,
              )
            ],
          )));
    }

    // opptin texte
    if (widget.articleForm['arrFeld']['optinActive'] == "1") {
      _formFields.add(Padding(
          padding: EdgeInsets.only(top: 10.00),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(padding: EdgeInsets.only(top: 10.00)),
                Text(
                  widget.article['arrTexte']['optin_text'],
                  style: Theme.of(context).textTheme.body1.merge(TextStyle()),
                ),
                FormBuilderSwitch(
                  activeColor: switchOn,
                  activeTrackColor: switchBackground,
                  inactiveThumbColor: switchOff,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.all(0)),
                  label: Text(""),
                  attribute: "optin",
                  initialValue: false,
                  onChanged: (value) {
                    setState(() {
                      _isOptin = value;
                    });
                  },
                ),
                _isOptin
                    ? FormBuilderRadio(
                        validators:
                            _validators(1, settings['optin']['required']),
                        activeColor: switchOn,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.all(0),
                            labelStyle: Theme.of(context)
                                .textTheme
                                .body1
                                .merge(TextStyle()),
                            labelText: _ifRequiredLabel(
                                widget.article['arrTexte']['optin_textinfo'],
                                1)),
                        attribute: "optininfo",
                        options: [
                            FormBuilderFieldOption(
                                label: null,
                                value: "te",
                                child: Text("Telefon",
                                    style: Theme.of(context)
                                        .textTheme
                                        .body1
                                        .merge(TextStyle()))),
                            FormBuilderFieldOption(
                                label: null,
                                value: "em",
                                child: Text("E-Mail",
                                    style: Theme.of(context)
                                        .textTheme
                                        .body1
                                        .merge(TextStyle()))),
                            FormBuilderFieldOption(
                                label: null,
                                value: "teem",
                                child: Text("Telefon und E-Mail",
                                    style: Theme.of(context)
                                        .textTheme
                                        .body1
                                        .merge(TextStyle())))
                          ])
                    : Container(),
              ])));
      _formFields.add(Padding(
          padding: EdgeInsets.only(top: 10.00),
          child: Divider(
            color: div,
            height: 1,
            thickness: 1,
          )));
    }

    // datenschutz
    if (widget.article['datenschutz_ob'] == "Y") {
      _formFields.add(Padding(
          padding: EdgeInsets.only(top: 10.00),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Html(
                    linkStyle:
                        Theme.of(context).textTheme.body1.merge(TextStyle(
                              decoration: TextDecoration.underline,
                            )),
                    data: settings['policytext'],
                    defaultTextStyle:
                        Theme.of(context).textTheme.body1.merge(TextStyle()),
                    customTextStyle: (dom.Node node, TextStyle baseStyle) {
                      if (node is dom.Element) {
                        switch (node.localName) {
                          case "a":
                            return baseStyle.merge(Theme.of(context)
                                .textTheme
                                .body1
                                .merge(TextStyle(
                                    decoration: TextDecoration.underline)));
                        }
                      }
                      return baseStyle;
                    },
                    onLinkTap: (url) {
                      // open url in a webview
                      _launchUrl(url);
                    }),
              ])));
    }

    /*
      List<dynamic> fields = widget.articleForm['arrFeld'];
      if (fields != null) {
        fields.asMap().forEach((index, item) {
          // List<DropdownMenuItem<dynamic>> values = item['wert'];

        });
      }
      */

    return new Column(children: _formFields);
  }

  Widget _getForm(settings, appSettings, sizingInformation, context, mydata) {
    bool isDark = MediaQuery.of(context).platformBrightness == Brightness.dark;

    Color bt;
    Color te;

    bt = Color(0XFF333333);
    te = Colors.white;

    if (isDark) {
      bt = Colors.white;
      te = Color(0XFF333333);
    }

    return SingleChildScrollView(
        controller: _scrollController,
        child: Center(
            child: Container(
                width: _getWidth(sizingInformation, appSettings),
                child: Padding(
                  padding: EdgeInsets.only(left: 20, right: 20, top: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(top: 15.00),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(widget.articleForm['text']['formTitel'],
                                  style: Theme.of(context)
                                      .textTheme
                                      .title
                                      .merge(TextStyle(
                                          fontSize: 22.00,
                                          fontWeight: FontWeight.w600))),
                            ],
                          )),
                      Column(children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 20),
                        ),
                        FormBuilder(
                            key: _fbKey,
                            initialValue: {
                              'date': DateTime.now(),
                              'accept_terms': false,
                            },
                            // autovalidate: true,
                            child: Column(
                              children: <Widget>[
                                _getFormFields(context, settings, appSettings,
                                    sizingInformation, mydata)
                              ],
                            )),
                        Padding(
                            padding: EdgeInsets.only(top: 30, bottom: 20),
                            child: RaisedButton(
                              child: /*_getSubmitButtonText(settings, te),*/
                                  Container(
                                      height: 50,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: <Widget>[
                                          _getSubmitButtonText(settings, te)
                                        ],
                                      )),
                              color: bt,
                              onPressed: () {
                                bool _valid = true;
                                if (!_fbKey.currentState.validate()) {
                                  switch (widget.type) {
                                    case "B":
                                    case "V":
                                      if (_file == null) {
                                        setState(() {
                                          _fileValid = false;
                                          _colorFileRequired = Colors.red;
                                        });
                                      } else {
                                        if (_colorFileRequired == Colors.red) {
                                          setState(() {
                                            _fileValid = true;
                                            _colorFileRequired = Colors.blue;
                                          });
                                        }
                                      }
                                      break;
                                    default:
                                      break;
                                  }
                                  // print(_fbKey.currentState);
                                  _scrollController.animateTo(0.00,
                                      duration: Duration(milliseconds: 1),
                                      curve: Curves.ease);
                                  _scaffoldKey.currentState
                                      .showSnackBar(SnackBar(
                                    content: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(settings['invalid'],
                                            style:
                                                TextStyle(color: Colors.white))
                                      ],
                                    ),
                                    backgroundColor: Colors.red,
                                  ));
                                } else {
                                  switch (widget.type) {
                                    case "B":
                                    case "V":
                                      if (_file == null) {
                                        _valid = false;
                                        setState(() {
                                          _fileValid = false;
                                          _colorFileRequired = Colors.red;
                                        });
                                      } else {
                                        if (_colorFileRequired == Colors.red) {
                                          setState(() {
                                            _fileValid = true;
                                            _colorFileRequired = Colors.blue;
                                          });
                                        }
                                      }
                                      break;
                                    default:
                                      break;
                                  }
                                  ;
                                  if (_valid) {
                                    _fbKey.currentState.save();
                                    // print(_fbKey.currentState.fields);
                                    _submitForm();
                                  }
                                  // _sendMyData();
                                }
                              },
                            )),
                        Padding(
                            padding: EdgeInsets.only(top: 30),
                            child: Container()),
                      ])
                    ],
                  ),
                ))));
  }

  void _showMyDataHelpDialog(settings) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return Container(
            child: Stack(
          children: <Widget>[
            GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: PopUpMyData()),
          ],
        ));
      },
    );
  }

  void _showDeleteDialog(settings) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(settings['reset']['title'],
              style: Theme.of(context).textTheme.title.merge(TextStyle())),
          content: new Text(settings['reset']['text'],
              style: Theme.of(context).textTheme.body1.merge(TextStyle())),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new RaisedButton(
              child: new Text(settings['reset']['yes'].toString().toUpperCase(),
                  style: TextStyle(
                      letterSpacing: 1.25,
                      fontFamily: 'Roboto-Bold',
                      fontSize: 12,
                      fontWeight: FontWeight.w500)),
              onPressed: () {
                _fbKey.currentState.reset();
                setState(() {
                  _auswahl = Map();
                  _colorFileRequired = Colors.blue;
                  _file = null;
                });
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
                _scrollController.animateTo(0.00,
                    duration: Duration(milliseconds: 1), curve: Curves.ease);
              },
            ),
            new RaisedButton(
              child: new Text(
                  settings['reset']['cancel'].toString().toUpperCase(),
                  style: TextStyle(
                      letterSpacing: 1.25,
                      fontFamily: 'Roboto-Bold',
                      fontSize: 12,
                      fontWeight: FontWeight.w500)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    dynamic settings = GlobalConfiguration().get("form");
    dynamic appSettings = GlobalConfiguration().get("app");
    dynamic myDataSettings = GlobalConfiguration().get("mydata");

    return BaseWidget(builder: (context, sizingInformation) {
      print(sizingInformation);
      return Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            /*
          leading: IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          */
            title: Text(widget.article['titel']),
            actions: <Widget>[
              IconButton(
                  icon: Icon(
                    Icons.delete,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    FocusScope.of(context).unfocus();
                    _showDeleteDialog(settings);
                  })
            ],
          ),
          body: FutureBuilder<dynamic>(
              future: _getMyData(myDataSettings),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return BaseWidget(builder: (context, sizingInformation) {
                    return _getForm(settings, appSettings, sizingInformation,
                        context, snapshot.data);
                  });
                } else if (snapshot.hasError) {
                  return Center(child: Text(snapshot.error));
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              }));
    });
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the widget tree.
    // This also removes the _printLatestValue listener.
    emailController.dispose();
    nameController.dispose();
    streetController.dispose();
    zipController.dispose();
    cityController.dispose();
    fonController.dispose();
    handyController.dispose();
    birthdayController.dispose();
    intnumberController.dispose();
    ibanController.dispose();
    bicController.dispose();
    _disposeVideoController();
    super.dispose();
  }
}

class DrawTriangle extends CustomPainter {
  Paint _paint;

  DrawTriangle(context) {
    _paint = Paint()
      ..color = Colors.amber
      ..style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Size size) {
    var path = Path();
    path.moveTo(size.width / 2, 6);
    path.lineTo(0, size.height);
    path.lineTo(size.height, size.width);
    path.close();
    canvas.drawPath(path, _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
