import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';

class PopUpMyData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var brightness = MediaQuery.of(context).platformBrightness;
    // print(brightness);
    dynamic settings = GlobalConfiguration().get("form");
    return Dialog(
        shape: BeveledRectangleBorder(
          borderRadius: BorderRadius.circular(0),
        ),
        elevation: 0.0,
        child: GestureDetector(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: IntrinsicWidth(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Flexible(
                    child: SingleChildScrollView(
                      child: Stack(children: <Widget>[
                        Positioned(
                          right: 10.0,
                          top: 10.0,
                          child: Align(
                            alignment: Alignment.topRight,
                            child: CircleAvatar(
                              radius: 14.0,
                              backgroundColor: Color(0XFF333333),
                              child: Icon(Icons.close, color: Colors.white),
                            ),
                          ),
                        ),
                        Padding(
                            padding: EdgeInsets.all(20),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                Text(
                                  settings['mydata']['title'],
                                  style: Theme.of(context)
                                      .textTheme
                                      .title
                                      .merge(TextStyle()),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 10),
                                ),
                                Text(settings['mydata']['helptext'],
                                    style: Theme.of(context)
                                        .textTheme
                                        .body1
                                        .merge(TextStyle())),
                              ],
                            )),
                      ]),
                    ),
                  )
                ],
              ),
            )));
  }
}
