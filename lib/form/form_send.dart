import 'package:flutter/material.dart';
import 'package:flutter_html/rich_text_parser.dart';
import 'dart:io' show Platform, File;
import 'dart:async';
import 'package:pendelix/ui/base_widget.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:device_info/device_info.dart';
import 'dart:convert';
import 'package:pendelix/home/home.dart';
import 'package:uuid/uuid.dart';
import 'package:path/path.dart' as path;

class ArticleFormSend extends StatefulWidget {
  const ArticleFormSend(
      {Key key,
      this.article,
      this.articleForm,
      this.type,
      this.data,
      this.file,
      this.lat,
      this.lng,
      this.auswahl,
      this.push})
      : super(key: key);

  final String type;
  final dynamic article;
  final dynamic articleForm;
  final File file;
  final String lat;
  final String lng;
  final Map auswahl;
  final dynamic push;
  final Map data;

  @override
  _ArticleFormSendState createState() => _ArticleFormSendState();
}

class _ArticleFormSendState extends State<ArticleFormSend>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String _whatState = 'loading';
  String _errorText = '';
  String _successText = '';
  NetworkCheck _networkCheck = NetworkCheck();
  MediaQueryData _queryData;
  DeviceInfoPlugin _deviceInfo = DeviceInfoPlugin();
  dynamic _settings;
  dynamic _appSettings;

  void initState() {
    _settings = GlobalConfiguration().get("form");
    _appSettings = GlobalConfiguration().get("app");
    widget.data.forEach((key, value) {
      print(key.toString() + ': ' + value.currentState.value.toString());
    });
    _submitData();
  }

  _submitData() {
    _networkCheck.checkInternet((isNetworkPresent) async {
      switch (isNetworkPresent) {
        case true:
          var uri = Uri.parse(_settings['url'][widget.type]);
          var request = new http.MultipartRequest("POST", uri);
          request.fields['id'] = widget.article['id'];
          if (Platform.isAndroid) {
            AndroidDeviceInfo androidInfo = await _deviceInfo.androidInfo;

            /*
            print(androidInfo.board);
            print(androidInfo.bootloader);
            print(androidInfo.brand);
            print(androidInfo.device);
            print(androidInfo.display);
            print(androidInfo.fingerprint);
            print(androidInfo.hardware);
            print(androidInfo.host);
            print(androidInfo.androidId);
            print(androidInfo.manufacturer);
            print(androidInfo.model);
            print(androidInfo.product);
            print(androidInfo.tags);
            print(androidInfo.type);
            print(androidInfo.version.release);
            print(androidInfo.version.sdkInt);
            */

            request.fields['deviceid'] = androidInfo.androidId;
            request.fields['devicemodel'] = androidInfo.model;
            request.fields['deviceplatform'] =
                "Android - " + androidInfo.version.release;
            request.fields['devicetoken'] = "";
            request.fields['deviceenv'] = "";

          } else if (Platform.isIOS) {
            IosDeviceInfo iosInfo = await _deviceInfo.iosInfo;
            request.fields['deviceid'] = iosInfo.name;
            request.fields['devicemodel'] = iosInfo.model;
            request.fields['deviceplatform'] = "Ios - " + iosInfo.systemVersion;
            request.fields['devicetoken'] = "";
            request.fields['deviceenv'] = "";
          }

          widget.data.forEach((key, value) {
            request.fields[key] = value.currentState.value.toString();
          });

          if (request.fields['optin'] != null &&
              request.fields['optininfo'] != null) {
            request.fields['optin'] = "1";
          }

          request.fields['lat'] = widget.lat != null ? widget.lat : "";
          request.fields['lng'] = widget.lng != null ? widget.lng : "";

          if (widget.auswahl.length > 0) {
            request.fields['auswahl'] = json.encode(widget.auswahl);
          }

          if (widget.file != null) {
            var bn = path.basename(widget.file.path);
            var bytes = widget.file.readAsBytesSync();
            var uuid = new Uuid();
            final multipartFile = http.MultipartFile.fromBytes(
                'file', await widget.file.readAsBytes(),
                filename: uuid.v4() + bn);
            request.files.add(multipartFile);
          }

          if (widget.push != null) {
            if(widget.push['token'] != null) {
              request.fields['pushtoken'] = widget.push['token'];
            }
          }

          try {

            // request.fields.forEach((key, value) {
            //  print(key.toString() + ': ' + value.toString());
            // });

            var stream = await request.send();
            var response = http.Response.fromStream(stream);
            response.then((onValue) {
              // print(onValue.body);
              if (onValue.statusCode == 200) {
                final dynamic resp = json.decode(onValue.body);
                if (resp['success'] == 1) {
                  setState(() {
                    _successText = widget.articleForm['text']['formBestText'];
                    _whatState = 'success';
                  });
                } else {
                  setState(() {
                    _errorText = resp['error']['message'];
                    _whatState = 'error';
                  });
                }
              } else {
                setState(() {
                  _whatState = 'offline';
                });
              }
            });
          } catch (e) {
            print(e);
            setState(() {
              _whatState = 'error';
            });
          }

          break;
        default:
          setState(() {
            _whatState = 'offline';
          });
          break;
      }
    });
  }

  Widget _switchState() {
    switch (_whatState) {
      case "offline":
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                height: 40,
                child: Icon(Icons.signal_wifi_off, size: 40)),
            Container(
              width: 200,
              child: Text(
                'Es besteht anscheinend keine Verbindung zum Internet',
                textAlign: TextAlign.center,
              ),
            ),
            MaterialButton(
              minWidth: 200,
              color: Colors.blue,
              onPressed: () {
                setState(() {
                  _submitData();
                });
              },
              child: Text(_settings['wifitryagain'],
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold)),
            )
          ],
        ));
        break;
      case "success":
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                height: 40,
                child: Icon(Icons.check, color: Colors.green, size: 40)),
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),
            Container(
              width: 200,
              child: Text(
                _successText,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ));
        break;
      case "error":
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                height: 40,
                child: Icon(Icons.error, size: 40)),
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),
            Container(
              width: 200,
              child: Text(
                'Ihre Daten konnten leider nicht übertragen werden',
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top:10),
            ),
            RaisedButton(
              onPressed: () {
                setState(() {
                  _submitData();
                });
              },
              child: Text("ERNEUT VERSUCHEN",
                  style: TextStyle(
                      letterSpacing: 1.25,
                      fontFamily: 'Roboto-Bold',
                      fontSize: 12,
                      fontWeight: FontWeight.w500)),
            )
          ],
        ));
        break;
      case "loading":
      default:
        return Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(height: 40, child: CircularProgressIndicator()),
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),
            Container(
              width: 200,
              child: Text('Ihre Daten werden übertragen...',
                  textAlign: TextAlign.center),
            ),
          ],
        ));
        break;
    }
  }

  _switchBack(context) {
    switch (_whatState) {
      case 'success':
        Navigator.of(context).popUntil((route) => route.isFirst);
        break;
      default:
        Navigator.of(context).pop(true);
        break;
    }
    return new Future<bool>.value(false);
  }

  Widget build(BuildContext context) {
    _queryData = MediaQuery.of(context);
    return new WillPopScope(
        onWillPop: () => _switchBack(context),
        child: BaseWidget(builder: (context, sizingInformation) {
          print(sizingInformation);
          return Scaffold(
              key: _scaffoldKey,
              appBar: AppBar(
                /*
          leading: IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          */
                title: Text(widget.article['titel']),
              ),
              body: _switchState());
        }));
  }
}

class NetworkCheck {
  Future<bool> check() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }

  dynamic checkInternet(Function func) {
    check().then((internet) {
      if (internet != null && internet) {
        func(true);
      } else {
        func(false);
      }
    });
  }
}
