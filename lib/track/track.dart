import 'package:flutter/material.dart';
import 'package:pendelix/drawer/drawer.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:pendelix/database/database.dart';
import 'package:pendelix/form/form.dart';
import 'package:pendelix/article/SABT.dart';
import 'package:pendelix/article/SABTI.dart';
import 'package:http/http.dart';
import 'package:flutter/foundation.dart';
import 'dart:convert';
import 'package:transparent_image/transparent_image.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;
import 'package:url_launcher/url_launcher.dart';
import 'package:pendelix/ui/base_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pendelix/track/track_video.dart';
import 'package:pendelix/track/elevation_chart.dart';

class Track extends StatefulWidget {
  const Track({Key key, this.article}) : super(key: key);

  final dynamic article;

  @override
  _TrackState createState() => _TrackState();
}

class _TrackState extends State<Track> with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  MediaQueryData _queryData;
  var _db;
  var _bottomIndex = 0;
  List _buttons;

  void initState() {
    dynamic settings = GlobalConfiguration().get("article");
    // print(_buttons);
    _buttonListInit(settings);
    _dbInit();
  }

  void _buttonListInit(settings) async {
    _buttons = await _buttonList(widget.article, settings);
    setState(() {
      _buttons = _buttons;
    });
  }

  Future _buttonList(dynamic item, settings) async {
    List buttons = List();
    if (item['bestellung_ob'] == 'Y') {
      buttons.add({
        "text":
        item['bestellung']['text']['formTitel'].toString().toUpperCase(),
        "type": "O",
        "icon": Icons.shopping_basket,
        "form": item['bestellung']
      });
    }
    if (item['nachricht_ob'] == 'Y') {
      buttons.add({
        "text": item['nachricht']['text']['formTitel'].toString().toUpperCase(),
        "type": "N",
        "icon": Icons.message,
        "form": item['nachricht']
      });
    }
    if (item['bild_ob'] == 'Y') {
      buttons.add({
        "text": item['bild']['text']['formTitel'].toString().toUpperCase(),
        "type": "B",
        "icon": Icons.image,
        "form": item['bild']
      });
    }
    if (item['video_ob'] == 'Y') {
      buttons.add({
        "text": item['video']['text']['formTitel'].toString().toUpperCase(),
        "type": "V",
        "icon": Icons.videocam,
        "form": item['video']
      });
    }
    /*if (item['audio_ob'] == 'Y') {
      buttons.add({
        "text": item['audio']['text']['formTitel'],
        "type": "A",
        "icon": Icons.audiotrack,
        "form": item['audio']
      });
    }
     */
    if (item['telefon_ob'] == 'Y') {
      buttons.add({
        "text": settings['phonetext'].toString().toUpperCase(),
        "type": "P",
        "icon": Icons.phone
      });
    }
    return buttons;
  }

  _buttonRender(Map button, bool isDark) {

    Color bt;
    Color te;

    bt = Color(0XFF333333);
    te = Colors.white;

    if(isDark){
      bt = Colors.white;
      te = Color(0XFF333333);
    }

    switch (button['type']) {
      case "P":
        return Padding(
            padding: EdgeInsets.only(bottom: 10),
            child: RaisedButton(
              // minWidth: 200,
                color: bt,
                onPressed: () {
                  _launchPhone(widget.article['telefon_nummer']);
                },
                child: Container(
                    height: 50,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(top: 0.00),
                            child: Icon(button['icon'],
                                color: te, size: 16.00)),
                        Padding(
                          padding: EdgeInsets.only(left: 15.00),
                        ),
                        Text(button['text'],
                            style:
                            TextStyle(color: te, fontSize: 14)),
                      ],
                    ))));
        break;
      default:
        return Padding(
            padding: EdgeInsets.only(bottom: 10),
            child: RaisedButton(
              // minWidth: 200,
              // color: Colors.blue,
                color: bt,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ArticleForm(
                            article: widget.article,
                            articleForm: button['form'],
                            type: button['type'])),
                  );
                },
                child: Container(
                    height: 50,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(top: 0.00),
                            child: Icon(button['icon'],
                                color: te, size: 16.00)),
                        Padding(
                          padding: EdgeInsets.only(left: 15.00),
                        ),
                        Text(button['text'],
                            style: TextStyle(
                                color: te,
                                fontSize: 14,
                                fontWeight: FontWeight.w500)),
                      ],
                    ))));
        break;
    }
  }

  void _dbInit() async {
    _db = new DatabaseHelper();
    await _db.getDb();
  }

  _getWidth(sizingInformation, appSettings) {
    if (sizingInformation.screenSize.width > appSettings['breakpoint']) {
      return appSettings['breakpoint'];
    } else {
      return sizingInformation.screenSize.width;
    }
  }

  /*
  Widget _article(item, queryData) {
    // 600 x 263
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          GestureDetector(
            onTap: () {},
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _responsiveImage(item, queryData),
                  _title(item, queryData),
                  _subtitle(item, queryData)
                ]),
          )
        ]);
  }
  */

  Widget _date(item, queryData) {
    switch (item['typ']) {
      case "aktion":
        return Padding(
          padding: EdgeInsets.only(left: 15, right: 15, top: 30, bottom: 0),
          /*child: Hero(
          tag: "titel" + item['id'],*/
          child: Text(item['datum'],
              style: Theme.of(context)
                  .textTheme
                  .overline
                  .merge(TextStyle(letterSpacing: 1))
            /*
        style: TextStyle(
          fontSize: 20,
          height: 1.20,
          fontFamily: 'clan-mediummedium',
          color: Colors.black87,
          fontWeight: FontWeight.w600,
        ),*/
            /*)*/),
        );
        break;
      case "lesermarkt":
        return Container();
        break;
    }
  }

  Widget _title(item, queryData) {
    double pad = 20;
    /*
    if(item['typ'] == "lesermarkt"){
      pad = 25;
    }
     */
    return Padding(
      padding: EdgeInsets.only(left: 15, right: 15, top: pad),
      /*child: Hero(
        tag: "title"+item['id'],*/
      child: Text(
        item['ti'],
        style: GoogleFonts.openSans(textStyle: Theme.of(context)
            .textTheme
            .title
            .merge(TextStyle(fontSize: 22.00, fontWeight: FontWeight.w800))),
      ),
      /*)*/
    );
  }

  Widget _text(item, queryData) {
    return Padding(
        padding: EdgeInsets.only(left: 15, right: 15, top: 5),
        child: Html(
          data: item['text'],
          onLinkTap: (url) {},
          customTextStyle: (dom.Node node, TextStyle baseStyle) {
            if (node is dom.Element) {
              switch (node.localName) {
                case "p":
                  return Theme.of(context).textTheme.body1.merge(TextStyle());
              }
            }
            return baseStyle;
          },
        ));
  }

  Widget _subtitle(item, queryData) {
    return Padding(
      padding: EdgeInsets.only(left: 15, right: 15, top: 0, bottom: 0),
      /*child: Hero(
    tag: "subtitle"+item['id'],*/
      child: Text(item['dc'],
          style: Theme.of(context).textTheme.body1.merge(TextStyle())),
      /*)*/
    );
  }

  Widget _responsiveImage(item, queryData, sizingInformation, appSettings) {
    var im = appSettings['image'];
    if (item[im] != false) {
      double h = appSettings['trackimageh'];
      double w = appSettings['trackimagew'];
      // double height = h / w * queryData.size.width;
      double height = h / w * sizingInformation.localWidgetSize.width;
      return Padding(
          padding: EdgeInsets.only(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
          ),
          child: LayoutBuilder(
              builder: (context, constraints) => Column(
                children: <Widget>[
                  // SizedBox(height: (constraints.maxHeight - constraints.minHeight) * 0.2,),
                  // _divider(queryData),
                  /*Hero(
                          tag: 'imgHero' + item['id'],
                          child: */
                  Row(
                    children: <Widget>[

                      /*
                      Container(
                        width: constraints.maxWidth,
                        height: height,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: CachedNetworkImageProvider(
                                  item['th']['m']['f'])),
                        ),
                      )*/

                      Container(
                          width: constraints.maxWidth,
                          height: height,
                          /*
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: CachedNetworkImageProvider(
                                            item['bild_900'])),
                                  ),
                                  */
                          child: Stack(
                            children: <Widget>[
                              Center(
                                  child: CachedNetworkImage(
                                    imageUrl: item['th']['m']['f'],
                                    placeholder: (context, url) =>
                                        CircularProgressIndicator(
                                          strokeWidth: 1,
                                          valueColor:
                                          new AlwaysStoppedAnimation<Color>(
                                              Color(0XFF009ee3)),
                                        ),
                                  )),
                              Center(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    ClipOval(
                                      child: Material(
                                        color: Colors.white, // button color
                                        child: InkWell(
                                          splashColor: Colors.red, // inkwell color
                                          child: SizedBox(width: 50, height: 50, child: Icon(Icons.play_arrow, color: Colors.blue, size: 30,)),
                                          onTap: () {
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(builder: (context) => TrackVideo(article: widget.article,)),
                                            );
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ))

                    ],
                    /*)*/
                  )
                ],
              )));
    } else {
      return Container();
    }
  }

  Widget _formButtons(queryData, settings, isDark) {
    if (_buttons != null) {
      return Padding(
          padding: EdgeInsets.only(left: 15, right: 15, top: 20, bottom: 40),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              for (var item in _buttons) _buttonRender(item, isDark)
              /*
                FlatButton(
                    // minWidth: 200,
                    // color: Colors.blue,
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ArticleForm(
                                article: widget.article,
                                articleForm: item['form'],
                                type: item['type'])),
                      );
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(item['text'],
                            style: TextStyle(
                                color: Colors.blue,
                                fontSize: 15,
                                fontWeight: FontWeight.w600)),
                        Padding(
                          padding: EdgeInsets.only(left: 15.00),
                        ),
                        Padding(
                            padding: EdgeInsets.only(top: 4.00),
                            child: Icon(item['icon'],
                                color: Colors.blue, size: 15.00)),
                      ],
                    ))*/
            ],
          ));
    }
    return Container();
  }

  _launchPhone(number) async {
    String nb = "tel:" + number;
    if (await canLaunch(nb)) {
      await launch(nb);
    } else {
      throw 'Could not launch $nb';
    }
  }

  Widget _bottomBar(item, queryData) {
    return BottomNavigationBar(
      backgroundColor: Colors.grey[900],
      type: BottomNavigationBarType.fixed,
      onTap: (int index) {
        setState(() {
          _bottomIndex = index;
        });
        switch (_bottomIndex) {
          case 0:
            if (item['nachricht_ob'] == "Y") {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ArticleForm(
                        article: widget.article,
                        articleForm: widget.article['nachricht'],
                        type: "N")),
              );
            }
            break;
          case 1:
            if (item['bild_ob'] == "Y") {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ArticleForm(
                        article: widget.article,
                        articleForm: widget.article['bild'],
                        type: "B")),
              );
            }
            break;
          case 2:
            if (item['video_ob'] == "Y") {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ArticleForm(
                        article: widget.article,
                        articleForm: widget.article['video'],
                        type: "V")),
              );
            }
            break;
          case 3:
            if (item['audio_ob'] == "Y") {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ArticleForm(
                        article: widget.article,
                        articleForm: widget.article['audio'],
                        type: "A")),
              );
            }
            break;
          case 4:
            if (item['telefon_ob'] == "Y") {
              _launchPhone(item['telefon_nummer']);
            }
            break;
        }
      },
      items: [
        _bottomBarItem(item['nachricht_ob'], "N",
            Icon(Icons.message, color: Colors.white)),
        _bottomBarItem(
            item['bild_ob'], "B", Icon(Icons.image, color: Colors.white)),
        _bottomBarItem(
            item['video_ob'], "V", Icon(Icons.videocam, color: Colors.white)),
        /*_bottomBarItem(
            item['audio_ob'], "A", Icon(Icons.audiotrack, color: Colors.white)),*/
        _bottomBarItem(
            item['telefon_ob'], "P", Icon(Icons.phone, color: Colors.white)),
      ],
    );
  }

  BottomNavigationBarItem _bottomBarItem(active, type, icon) {
    double op = 0.9;
    switch (active) {
      case "Y":
        break;
      case "N":
        op = 0.2;
        break;
    }
    return new BottomNavigationBarItem(
        icon: Opacity(opacity: op, child: icon),
        title: Text(
          '',
          style: TextStyle(fontWeight: FontWeight.bold, height: 0.0),
        ),
        activeIcon: Opacity(opacity: op, child: icon),
        backgroundColor: Colors.black87);
  }

  Widget _elevationChart(article, sizingInformation) {
    return Container(
        height: 500,
        child: ElevationChart(article: article, sizingInformation: sizingInformation));
  }

  @override
  Widget build(BuildContext context) {
    bool isDark = MediaQuery.of(context).platformBrightness == Brightness.dark;

    dynamic settings = GlobalConfiguration().get("article");
    dynamic appSettings = GlobalConfiguration().get("app");
    _queryData = MediaQuery.of(context);

    return BaseWidget(builder: (context, sizingInformation) {
      double h = appSettings['trackimageh'];
      double w = appSettings['trackimagew'];
      double height = h / w * sizingInformation.localWidgetSize.width;
      return Scaffold(
        key: _scaffoldKey,
        body: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  leading: SABTI(),
                  /*
                  leading: Padding(
                      padding: EdgeInsets.only(left: 10),
                      child: SizedBox(
                        height: 10,
                        width: 10,
                        child: CircleAvatar(
                          maxRadius: 10,
                          minRadius: 10,
                          backgroundColor: Color.fromRGBO(0, 0, 0, 0.2),
                          child: BackButton(
                            color: Colors.white,
                          ),
                        ),
                      )),*/
                  expandedHeight: height - _queryData.padding.top,
                  title: SABT(child: Text(widget.article['ti'])),
                  floating: true,
                  pinned: true,
                  flexibleSpace: FlexibleSpaceBar(
                    centerTitle: true,
                    background: _responsiveImage(widget.article, _queryData,
                        sizingInformation, appSettings),
                    /*
                    background: Image.network(
                      widget.article['bild_600'],
                      fit: BoxFit.cover,
                    )
                    */
                  ),
                ),
              ];
            },
            body: SingleChildScrollView(
                child: Center(
                    child: Container(
                      width: _getWidth(sizingInformation, appSettings),
                      child: new Stack(children: <Widget>[
                        Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              // _date(widget.article, _queryData),
                              _title(widget.article, _queryData),
                              Padding(
                                padding: EdgeInsets.only(top: 15),
                              ),
                              _subtitle(widget.article, _queryData),
                              _elevationChart(widget.article, sizingInformation),
                              // _text(widget.article, _queryData),
                              // _formButtons(_queryData, settings, isDark),
                            ])
                      ]),
                    )))),
        /*
        body: Expanded(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
              GestureDetector(
                onTap: () {},
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      _responsiveImage(widget.article, _queryData),
                      _title(widget.article, _queryData),
                      _subtitle(widget.article, _queryData),
                      _text(widget.article, _queryData)
                    ]),
              )
            ]))
            */
        /* bottomNavigationBar: _bottomBar(widget.article, _queryData)*/
      );
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
}
