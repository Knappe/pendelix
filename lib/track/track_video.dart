import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:pendelix/database/database.dart';
import 'package:flutter/foundation.dart';
import 'package:pendelix/ui/base_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class TrackVideo extends StatefulWidget {
  const TrackVideo({Key key, this.article}) : super(key: key);

  final dynamic article;

  @override
  _TrackVideoState createState() => _TrackVideoState();
}

class _TrackVideoState extends State<TrackVideo> with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  MediaQueryData _queryData;
  var _db;
  String videoId;
  YoutubePlayerController _ytcontroller;

  void initState() {
    dynamic settings = GlobalConfiguration().get("article");
    videoId = YoutubePlayer.convertUrlToId(widget.article['vi']);
    // print(_buttons);
    _dbInit();
    _ytcontroller = YoutubePlayerController(
      initialVideoId: videoId,
      flags: YoutubePlayerFlags(
        mute: false,
        autoPlay: true,
        forceHideAnnotation: true,
      ),
    );
  }

  void _dbInit() async {
    _db = new DatabaseHelper();
    await _db.getDb();
  }

  _getWidth(sizingInformation, appSettings) {
    if (sizingInformation.screenSize.width > appSettings['breakpoint']) {
      return appSettings['breakpoint'];
    } else {
      return sizingInformation.screenSize.width;
    }
  }

  @override
  Widget build(BuildContext context) {
    bool isDark = MediaQuery.of(context).platformBrightness == Brightness.dark;

    dynamic settings = GlobalConfiguration().get("article");
    dynamic appSettings = GlobalConfiguration().get("app");
    _queryData = MediaQuery.of(context);

    return BaseWidget(builder: (context, sizingInformation) {
      double h = appSettings['trackimageh'];
      double w = appSettings['trackimagew'];
      double height = h / w * sizingInformation.localWidgetSize.width;
      return Scaffold(
        key: _scaffoldKey,
          appBar: AppBar(
          ),
        body:Center(
        child: YoutubePlayer(
          controller: _ytcontroller,
          showVideoProgressIndicator: true,
          progressIndicatorColor: Colors.blueAccent,
          topActions: <Widget>[
            SizedBox(width: 8.0),
            Expanded(
              child: Text(
                _ytcontroller.metadata.title,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18.0,
                ),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.settings,
                color: Colors.black,
                size: 25.0,
              ),
              onPressed: () {
                // _showSnackBar('Settings Tapped!');
              },
            ),
          ],
          onReady: () {
            // print('Player is ready.');
          },
        )));
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
}
