import 'package:flutter/material.dart';
import 'package:pendelix/drawer/drawer.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:pendelix/database/database.dart';
import 'package:pendelix/track/track.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import 'dart:convert';
import 'dart:io';
import 'package:pendelix/ui/base_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:pendelix/texts/texts.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:pendelix/home/popup.dart';
import 'package:google_fonts/google_fonts.dart';

// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:extended_image/extended_image.dart';

/*
Future<void> _downloadConfig(settings) async {
  String _dir = (await getApplicationDocumentsDirectory()).path;
  var req = await http.Client().get(Uri.parse(settings['apiconfig']));
  var file = File('$_dir/_settings.json');
  print(file);
  return file.writeAsBytes(req.bodyBytes);
}
*/

dynamic _parseAllArticles(String responseBody) {
  // print(responseBody);
  final parsed = json.decode(responseBody);
  return parsed;
}

class TrackList extends StatefulWidget {
  TrackList({Key key}) : super(key: key);

  @override
  _TrackListState createState() => _TrackListState();
}

class _TrackListState extends State<TrackList> with SingleTickerProviderStateMixin {
  TabController _tabController;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  MediaQueryData _queryData;
  var _db;
  Future<dynamic> _articles;
  String _co = 'nix';
  String _dir;

  void initState() {
    dynamic settings = GlobalConfiguration().get("main");
    // dynamic down = _downloadConfig(settings);
    // _db = new DatabaseHelper();
    _dbInit();
    _tabController = new TabController(vsync: this, length: 2);
    _articles = _getAllArticles();
    // _articles.then((_articles) {
    //  _saveArticlesForOffline(_articles);
    // });
    // _saveArticlesForOffline(_articles);
    _dbInit();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => _showPopUp(context, settings));
  }

  _showPopUp(context, settings) async {
    List po = await _db.getMyPopUp();
    if (po[0]['ifpopup'] == 0) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return Container(
              child: Stack(
                children: <Widget>[
                  GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: PopUp()),
                ],
              ));
        },
      );
      _db.updateMyPopUp();
    }
  }

  void _dbInit() async {
    _db = new DatabaseHelper();
    await _db.getDb();
  }

  Future _saveArticlesForOffline(articles) async {
    await _db.getDb();
    List ls = await _db.getLastSync();
    if (ls != null) {
      var lastsync = ls[0]['lastsync'];
      // print(lastsync.toString() + " - " + articles['letzteaenderung'].toString());
      if (lastsync < articles['letzteaenderung']) {
        // print('jojojo');
      } else {
        // print('nönö');
      }
    }
  }

  Future<dynamic> _getAllArticles() async {
    // await _db.getDb();
    dynamic settings = GlobalConfiguration().get("main");
    final response = await http.get(settings['apiall']);
    // print(response);
    if (response.statusCode == 200) {
      return json.decode(response.body);
      // return compute(_parseAllArticles, response.body);
    } else {
      return false;
    }
  }

  _getWidth(sizingInformation, appSettings) {
    if (sizingInformation.screenSize.width > appSettings['breakpoint']) {
      return appSettings['breakpoint'];
    } else {
      return sizingInformation.screenSize.width;
    }
  }

  Widget _articleList(type, data, queryData, sizingInformation, appSettings) {
    var count;
    var item;
    List dataRel;
    dataRel = data['it'];
    // print(dataRel);
    count = dataRel.length;
    /*
    switch (type) {
      case "readermarket":
        count = data['countLesermarkt'];
        // print(count);
        dataRel = data['items']['lesermarkt'];
        break;
      case "actions":
      default:
        count = data['countAktionen'];
        dataRel = data['items']['aktion'];
        break;
    }
     */
    return SingleChildScrollView(
      // physics: AlwaysScrollableScrollPhysics(),
        child: Center(
            child: Container(
                width: _getWidth(sizingInformation, appSettings),
                child: ListView.separated(
                    primary: false,
                    separatorBuilder: (context, index) => _divider(queryData),
                    physics: NeverScrollableScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: count,
                    itemBuilder: (BuildContext context, int i) {
                      final index = i;
                      var item;
                      item = dataRel[index];
                      double pt = 0;
                      if (i == 0) {
                        pt = 25;
                      }
                      return Padding(
                        padding: EdgeInsets.only(top: pt),
                        child: Column(
                          children: <Widget>[
                            BaseWidget(builder: (context, sizingInformation) {
                              return _article(item, queryData,
                                  sizingInformation, appSettings, type);
                            }),
                          ],
                        ),
                      );
                    }))));

  }

  Widget _article(item, queryData, sizingInformation, appSettings, type) {
    // 600 x 263
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Track(article: item)),
              );
            },
            child:
            Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _responsiveImage(
                      item, queryData, sizingInformation, appSettings),
                  // _date(item, queryData, appSettings, type),
                  _title(item, queryData, appSettings),
                  _subtitle(item, queryData, appSettings, type),
                  _more(item, queryData, appSettings, type)
                ]),
          )
        ]);
  }

  Widget _more(item, queryData, appSettings, type) {
    return Padding(
        padding: EdgeInsets.only(left: 15, right: 15, bottom: 25, top: 5),
        child: RaisedButton(
          elevation: 0,
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Track(article: item)),
            );
          },
          child: Text(
            'DETAILS ANZEIGEN',
            style: TextStyle(
                letterSpacing: 1.25,
                fontFamily: 'Roboto-Bold',
                fontSize: 12,
                fontWeight: FontWeight.w500),
          ),
        ));
    return RaisedButton(
      child: Text('Mehr erfahren'),
    );
    switch (type) {
      case "actions":
        return Padding(
          padding: EdgeInsets.only(left: 15, right: 15, top: 20),
          /*child: Hero(
          tag: "titel" + item['id'],*/
          child: Text(item['datum'],
              style: Theme.of(context)
                  .textTheme
                  .overline
                  .merge(TextStyle(letterSpacing: 1))
            /*
        style: TextStyle(
          fontSize: 20,
          height: 1.20,
          fontFamily: 'clan-mediummedium',
          color: Colors.black87,
          fontWeight: FontWeight.w600,
        ),*/
            /*)*/),
        );
        break;
      case "readermarket":
        return Container();
        break;
    }
  }

  Widget _date(item, queryData, appSettings, type) {
    switch (type) {
      case "actions":
        return Padding(
          padding: EdgeInsets.only(left: 15, right: 15, top: 20),
          /*child: Hero(
          tag: "titel" + item['id'],*/
          child: Text(item['datum'],
              style: Theme.of(context)
                  .textTheme
                  .overline
                  .merge(TextStyle(letterSpacing: 1))
            /*
        style: TextStyle(
          fontSize: 20,
          height: 1.20,
          fontFamily: 'clan-mediummedium',
          color: Colors.black87,
          fontWeight: FontWeight.w600,
        ),*/
            /*)*/),
        );
        break;
      case "readermarket":
        return Container();
        break;
    }
  }

  Widget _title(item, queryData, appSettings) {
    return Padding(
      padding: EdgeInsets.only(left: 15, right: 15, top: 15),
      /*child: Hero(
          tag: "titel" + item['id'],*/
      child: Text(item['ti'],
          style: GoogleFonts.openSans(textStyle: Theme.of(context).textTheme.title.merge(TextStyle(
              fontSize: 20.00, height: 1.25, fontWeight: FontWeight.w800)))
        /*
        style: TextStyle(
          fontSize: 20,
          height: 1.20,
          fontFamily: 'clan-mediummedium',
          color: Colors.black87,
          fontWeight: FontWeight.w600,
        ),*/
        /*)*/),
    );
  }

  Widget _subtitle(item, queryData, appSettings, type) {

        return Padding(
          padding: EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
          /*child: Hero(
          tag: "subtitle" + item['id'],*/
          child: Text(
            item['dc'],
            style: GoogleFonts.openSans(textStyle: Theme.of(context).textTheme.body1.merge(TextStyle())),
          ),
          /*)*/
        );

  }

  Widget _divider(queryData) {
    return Padding(
      padding: EdgeInsets.only(left: 15, right: 15, bottom: 25),
      child: Divider(
        height: 1,
        thickness: 1,
      ),
    );
    Divider();

    switch (_co) {
      case "yellow":
        _co = "blue";
        return Container(
          width: queryData.size.width,
          height: 4,
          color: Colors.blue,
        );
        break;
      case "blue":
        _co = "yellow";
        return Container(
          width: queryData.size.width,
          height: 4,
          color: Colors.amber,
        );
        break;
      default:
        _co = "blue";
        return Container(height: 0);
        break;
    }
  }

  Widget _responsiveImage(item, queryData, sizingInformation, appSettings) {
    var im = appSettings['image'];
    if (item['th'] != null) {
      double h = appSettings['trackimageh'];
      double w = appSettings['trackimagew'];
      // double height = h / w * queryData.size.width;
      double height;
      if (item['th']['m']['w'] != 0) {
        // h = double.parse(item['th']['m']['h']);
        // w = double.parse(item['th']['m']['w']);
        h = double.parse(item['th']['m']['h'].toString());
        w = double.parse(item['th']['m']['w'].toString());
      }
      height = h / w * sizingInformation.localWidgetSize.width;
      /*
      print(h);
      print(w);
      print(height);
      print(sizingInformation.localWidgetSize.width);
      print('----');
      */
      return Padding(
          padding: EdgeInsets.only(
            top: 0,
            left: 15,
            right: 15,
            bottom: 0,
          ),
          child: LayoutBuilder(
              builder: (context, constraints) => Column(
                children: <Widget>[
                  // SizedBox(height: (constraints.maxHeight - constraints.minHeight) * 0.2,),
                  // _divider(queryData),
                  /*Hero(
                          tag: 'imgHero' + item['id'],
                          child: */
                  Row(
                    children: <Widget>[
                      Container(
                          width: constraints.maxWidth,
                          height: height,
                          /*
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: CachedNetworkImageProvider(
                                            item['bild_900'])),
                                  ),
                                  */
                          child: Stack(
                            children: <Widget>[
                              Center(
                                  child: CachedNetworkImage(
                                    imageUrl: item['th']['m']['f'],
                                    placeholder: (context, url) =>
                                        CircularProgressIndicator(
                                          strokeWidth: 1,
                                          valueColor:
                                          new AlwaysStoppedAnimation<Color>(
                                              Color(0XFF009ee3)),
                                        ),
                                  )),
                              Positioned(
                                left: 5,
                                bottom: 20,
                                child: Row(
                                  children: <Widget>[
                                    /*
                                    ClipOval(
                                      child: Material(
                                        color: Colors.white, // button color
                                        child: InkWell(
                                          splashColor: Colors.red, // inkwell color
                                          child: SizedBox(width: 30, height: 30, child: Icon(Icons.play_arrow, color: Colors.blue)),
                                          onTap: () {},
                                        ),
                                      ),
                                    ),
                                    */
                                    Icon(Icons.play_arrow, color: Colors.blue, size: 30),
                                    Padding(
                                      padding: EdgeInsets.only(left: 0)
                                    ),
                                    Text(
                                      item['vd'].toString() + ' min',
                                      style: TextStyle(
                                          color: Colors.white,
                                          letterSpacing: 1.25,
                                          fontFamily: 'Roboto-Bold',
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(left: 15)
                                    ),
                                    Icon(Icons.timer, color: Colors.blue, size: 30,),
                                    Padding(
                                        padding: EdgeInsets.only(left: 0)
                                    ),
                                    Text(
                                      item['stats']['ap']['duration'].toString() + ' min',
                                      style: TextStyle(
                                          color: Colors.white,
                                          letterSpacing: 1.25,
                                          fontFamily: 'Roboto-Bold',
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(left: 15)
                                    ),
                                    Icon(Icons.timeline, color: Colors.blue, size: 30,),
                                    Padding(
                                        padding: EdgeInsets.only(left: 0)
                                    ),
                                    Text(
                                        item['stats']['ap']['distance'].toString() + ' km',
                                      style: TextStyle(
                                          color: Colors.white,
                                          letterSpacing: 1.25,
                                          fontFamily: 'Roboto-Bold',
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(left: 15)
                                    ),
                                    Icon(Icons.terrain, color: Colors.blue, size: 30,),
                                    Padding(
                                        padding: EdgeInsets.only(left: 2)
                                    ),
                                    Text(
                                      item['stats']['ra']['minAltitude'].toString() + ' - ',
                                      style: TextStyle(
                                          color: Colors.white,
                                          letterSpacing: 1.25,
                                          fontFamily: 'Roboto-Bold',
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Text(
                                      item['stats']['ra']['maxAltitude'].toString() + ' m',
                                      style: TextStyle(
                                          color: Colors.white,
                                          letterSpacing: 1.25,
                                          fontFamily: 'Roboto-Bold',
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ))
                    ],
                    /*)*/
                  )
                ],
              )));
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    dynamic settings = GlobalConfiguration().get("main");
    dynamic appSettings = GlobalConfiguration().get("app");
    _queryData = MediaQuery.of(context);

    return BaseWidget(builder: (context, sizingInformation) {
      return Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
              title: Text(
                      'Routen',
                    ),
              actions: <Widget>[
                IconButton(
                  icon: const Icon(Icons.refresh),
                  tooltip: settings['refresh'],
                  onPressed: () {
                    setState(() {
                      _articles = Future<dynamic>.value(null);
                    });
                    Future.delayed(const Duration(milliseconds: 100), () {
                      setState(() {
                        _articles = _getAllArticles();
                      });
                    });
                  },
                ),
                /*
              IconButton(
                icon: const Icon(Icons.info_outline),
                tooltip: settings['about'],
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => TextAbout()),
                  );
                },
              ),
              */
              ],
              /*
              bottom: PreferredSize(
                preferredSize: Size(51, 51),
                child: Column(
                  children: <Widget>[
                    Divider(
                      height: 1,
                      color: Color(0XFF808080),
                    ),
                    new TabBar(
                      tabs: <Tab>[
                        new Tab(text: settings['actions']
                          // text: settings['actions'],
                        ),
                        new Tab(text: settings['readermarket']),
                      ],
                      controller: _tabController,
                    ),
                  ],
                ),
              )*/),
          body: FutureBuilder<dynamic>(
              future: _articles,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return new SingleChildScrollView(
                    child: _articleList('actions', snapshot.data, _queryData,
                        sizingInformation, appSettings),
                  );
                } else if (snapshot.hasError) {
                  return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.signal_wifi_off,
                            size: 40,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top:10),
                          ),
                          Container(
                            width: 200,
                            child: Text(
                              'Es besteht anscheinend keine Verbindung zum Internet',
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top:10),
                          ),
                          RaisedButton(
                            onPressed: () {
                              setState(() {
                                _articles = Future<dynamic>.value(null);
                              });
                              Future.delayed(const Duration(milliseconds: 100), () {
                                setState(() {
                                  _articles = _getAllArticles();
                                });
                              });
                            },
                            child: Text(settings['wifitryagain'].toString().toUpperCase(),
                                style: TextStyle(
                                    letterSpacing: 1.25,
                                    fontFamily: 'Roboto-Bold',
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500)),
                          )
                        ],
                      ));
                } else {
                  return Center(
                    child: CircularProgressIndicator(
                        valueColor: new AlwaysStoppedAnimation<Color>(
                            Color(0XFF009ee3))),
                  );
                }
              }));
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
}

/*
class DrawTriangle extends CustomPainter {
  Paint _paint;

  DrawTriangle(context) {
    _paint = Paint()
      ..color = Theme.of(context).scaffoldBackgroundColor
      ..style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Size size) {
    var path = Path();
    path.moveTo(size.width / 2, 6);
    path.lineTo(0, size.height);
    path.lineTo(size.height, size.width);
    path.close();
    canvas.drawPath(path, _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
 */
