import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:pendelix/database/database.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:pendelix/ui/base_widget.dart';
import 'package:pendelix/firebase/push.dart';
import 'package:flutter_custom_dialog/flutter_custom_dialog.dart';

class MyData extends StatefulWidget {
  MyData({Key key}) : super(key: key);

  @override
  _MyDataState createState() => _MyDataState();
}

class _MyDataState extends State<MyData> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _dateFormat = DateFormat("yyyy-MM-dd");
  ScrollController _scrollController = new ScrollController();
  var _db;

  TextEditingController emailController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController streetController = TextEditingController();
  TextEditingController zipController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController fonController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  TextEditingController birthdayController = TextEditingController();
  TextEditingController intnumberController = TextEditingController();
  TextEditingController ibanController = TextEditingController();
  TextEditingController bicController = TextEditingController();

  bool _isPush = false;
  bool _hasMyData = false;
  String _sent = 'start';
  dynamic _myData;
  dynamic _myPush;
  MediaQueryData _queryData;

  void initState() {
    _dbInit();
    _getMyData();
    _getPushData();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Future.delayed(const Duration(milliseconds: 100), () {
        FocusScope.of(context).unfocus();
        _scrollController.animateTo(0.00,
            duration: Duration(milliseconds: 1), curve: Curves.ease);
      });
    });
  }

  void _dbInit() async {
    _db = new DatabaseHelper();
    await _db.getDb();
  }

  Future _getPushData() async {
    String token = await PushNotificationsManager().getToken();
    List pu = await _db.getMyPush();
    // print(pu);
    // print(token);
    if (pu != null) {
      if (token == null) {
        _db.deleteMyPush();
      } else {
        _myPush = pu[0];
        if (_myPush['ifpush'] == 1) {
          setState(() {
            _isPush = true;
          });
        }
      }
    } else {
      if (token == null) {
        await _db._db.deleteMyPush();
        List pu = await _db.getMyPush();
        setState(() {
          _isPush = false;
        });
      }
    }
  }

  Future _pushSubscribe() async {
    try {
      setState(() {
        _isPush = true;
      });
      String token = await PushNotificationsManager().subscribe();
      if (token != null) {
        await _db.updateMyPush(token);
      } else {
        setState(() {
          _isPush = false;
        });
        _showPushError();
      }
    } catch (e) {

    }
  }

  Future _pushUnsubscribe() async {
    try {
      await PushNotificationsManager().unsubscribe();
      _db.deleteMyPush();
      setState(() {
        _isPush = false;
      });
    } catch (e) {}
  }

  Future _deleteMyData() async {
    await _db.deleteMyData();
    await _getMyData();
    emailController.value = emailController.value.copyWith(text: '');
    nameController.value = emailController.value.copyWith(text: '');
    streetController.value = streetController.value.copyWith(text: '');
    zipController.value = zipController.value.copyWith(text: '');
    cityController.value = cityController.value.copyWith(text: '');
    fonController.value = fonController.value.copyWith(text: '');
    mobileController.value = mobileController.value.copyWith(text: '');
    birthdayController.value = birthdayController.value.copyWith(text: '');
    intnumberController.value = intnumberController.value.copyWith(text: '');
    ibanController.value = ibanController.value.copyWith(text: '');
    bicController.value = bicController.value.copyWith(text: '');
    setState(() {
      _hasMyData = false;
      _sent = 'start';
    });
  }

  Future _sendMyData() async {
    Map<String, dynamic> toJson() => {
          'email': emailController.value.text,
          'name': nameController.value.text,
          'street': streetController.value.text,
          'zip': zipController.value.text,
          'city': cityController.value.text,
          'fon': fonController.value.text,
          'mobile': mobileController.value.text,
          'birthday': birthdayController.value.text,
          'intnumber': intnumberController.value.text,
          'iban': ibanController.value.text,
          'bic': bicController.value.text,
          'push': _isPush
        };
    await _db.updateMyData(jsonEncode(toJson()));
    setState(() {
      _hasMyData = true;
      _sent = 'sent';
    });
  }

  void _showPushError() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return Dialog(
            shape: BeveledRectangleBorder(
              borderRadius: BorderRadius.circular(0),
            ),
            elevation: 0.0,
            child: GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: IntrinsicWidth(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Flexible(
                        child: SingleChildScrollView(
                          child: Stack(children: <Widget>[
                            Positioned(
                              right: 10.0,
                              top: 10.0,
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                                child: Align(
                                  alignment: Alignment.topRight,
                                  child: CircleAvatar(
                                    radius: 14.0,
                                    backgroundColor: Color(0XFF333333),
                                    child:
                                        Icon(Icons.close, color: Colors.white),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: <Widget>[
                                    Text(
                                      "Fehler",
                                      style: Theme.of(context)
                                          .textTheme
                                          .title
                                          .merge(TextStyle()),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(top: 10),
                                    ),
                                    Text(
                                        "Wir konnten Sie leider nicht für Push-Nachrichten registrieren.",
                                        style: Theme.of(context)
                                            .textTheme
                                            .body1
                                            .merge(TextStyle())),
                                    Padding(
                                      padding: EdgeInsets.only(top: 10),
                                    ),
                                    Text(
                                        "Bitte verbinden Sie Ihr Gerät mit dem Internet.",
                                        style: Theme.of(context)
                                            .textTheme
                                            .body1
                                            .merge(TextStyle())),
                                  ],
                                )),
                          ]),
                        ),
                      )
                    ],
                  ),
                )));
      },
    );
  }

  void _showDeleteDialog(settings) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(settings['reset']['title']),
          content: new Text(
            settings['reset']['text'],
            style: Theme.of(context).textTheme.body1.merge(TextStyle()),
          ),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new RaisedButton(
              child: new Text(
                settings['reset']['yes'].toString().toUpperCase(),
                style: TextStyle(
                    letterSpacing: 1.25,
                    fontFamily: 'Roboto-Bold',
                    fontSize: 12,
                    fontWeight: FontWeight.w500),
              ),
              onPressed: () {
                _formKey.currentState.reset();
                _deleteMyData();
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
                _scrollController.animateTo(0.00,
                    duration: Duration(milliseconds: 1), curve: Curves.ease);
              },
            ),
            new RaisedButton(
              child: new Text(
                settings['reset']['cancel'].toString().toUpperCase(),
                style: TextStyle(
                    letterSpacing: 1.25,
                    fontFamily: 'Roboto-Bold',
                    fontSize: 12,
                    fontWeight: FontWeight.w500),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future _getMyData() async {
    List myData;
    myData = await _db.getMyData();
    if (myData != null) {
      if (myData[0]['items'] != null) {
        _myData = json.decode(myData[0]['items']);
        emailController.value =
            emailController.value.copyWith(text: _myData['email']);
        nameController.value =
            emailController.value.copyWith(text: _myData['name']);
        streetController.value =
            streetController.value.copyWith(text: _myData['street']);
        zipController.value =
            zipController.value.copyWith(text: _myData['zip']);
        cityController.value =
            cityController.value.copyWith(text: _myData['city']);
        fonController.value =
            fonController.value.copyWith(text: _myData['fon']);
        mobileController.value =
            mobileController.value.copyWith(text: _myData['mobile']);
        birthdayController.value =
            birthdayController.value.copyWith(text: _myData['birthday']);
        intnumberController.value =
            intnumberController.value.copyWith(text: _myData['intnumber']);
        ibanController.value =
            ibanController.value.copyWith(text: _myData['iban']);
        /*bicController.value =
            bicController.value.copyWith(text: _myData['bic']);*/
        setState(() {
          // _isPush = _myData['push'];
          _hasMyData = true;
        });
      }
    }
  }

  Widget _btnDeleteMyData(settings) {
    if (_hasMyData) {
      return IconButton(
        icon: Icon(Icons.delete),
        onPressed: () {
          _showDeleteDialog(settings);
        },
      );
    } else {
      return Container();
    }
  }

  Widget _saveButton(settings, isDark) {
    Color bt;
    Color te;

    bt = Color(0XFF333333);
    te = Colors.white;

    if (isDark) {
      bt = Colors.white;
      te = Color(0XFF333333);
    }

    if (_sent == 'start') {
      return Padding(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: RaisedButton(
              // minWidth: 200,
              color: bt,
              onPressed: () {
                setState(() {
                  _sent = 'sending';
                  _sendMyData();
                });
              },
              child: Container(
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(settings['save'].toString().toUpperCase(),
                          style: TextStyle(color: te, fontSize: 14)),
                    ],
                  ))));
    } else if (_sent == 'sending') {
      return Padding(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: RaisedButton(
              // minWidth: 200,
              color: bt,
              onPressed: () {},
              child: Container(
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                          height: 20.0,
                          width: 20.0,
                          child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(Colors.blue),
                              strokeWidth: 2.0)),
                    ],
                  ))));
    } else if (_sent == 'sent') {
      return Padding(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: RaisedButton(
              // minWidth: 200,
              color: bt,
              onPressed: () {
                setState(() {
                  _sent = 'start';
                });
              },
              child: Container(
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                          height: 20.0,
                          width: 20.0,
                          child: Icon(
                            Icons.done,
                            color: Colors.lightGreen,
                          )),
                    ],
                  ))));

      return MaterialButton(
          minWidth: 200,
          color: Colors.lightGreen,
          onPressed: () {
            setState(() {
              _sent = 'start';
            });
          },
          child: SizedBox(height: 20.0, width: 20.0, child: Icon(Icons.done)));
    } else {
      return Container();
    }
  }

  _getWidth(sizingInformation, appSettings) {
    if (sizingInformation.screenSize.width > appSettings['breakpoint']) {
      return appSettings['breakpoint'];
    } else {
      return sizingInformation.screenSize.width;
    }
  }

  _getPushSwitch(){
    return Text('');
  }

  Widget _getForm(settings, appSettings, sizingInformation, queryData) {
    bool isDark = MediaQuery.of(context).platformBrightness == Brightness.dark;

    Color switchBackground;
    Color switchOn;
    Color switchOff;
    Color div;

    switchBackground = Color(0XFF808080);
    switchOn = Color(0XFF333333);
    switchOff = Color(0XFFf1f1f1);

    if (isDark) {
      switchBackground = Color(0XFF808080);
      switchOn = Color(0XFFf1f1f1);
      switchOff = Color(0XFF333333);
    }

    return SingleChildScrollView(
        controller: _scrollController,
        child: Center(
            child: Container(
                width: _getWidth(sizingInformation, appSettings),
                child: Padding(
                    padding: EdgeInsets.only(
                        left: 20, right: 20, top: 20, bottom: 20),
                    child: Form(
                      key: _formKey,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Padding(padding: EdgeInsets.only(top: 10)),
                            Text(
                                'Speichere an dieser Stelle einmalig Deine Kontaktdaten. Bei zukünftiger Verwendung der App brauchst Du die hier hinterlegten Daten dann nicht mehr neu einzugeben. (Die Daten werden nur lokal auf Deinem Gerät gespeichert.)'),
                            Padding(padding: EdgeInsets.only(top: 25)),
                            TextFormField(
                              style: Theme.of(context)
                                  .textTheme
                                  .body1
                                  .merge(TextStyle(height: 1)),
                              controller: emailController,
                              decoration: InputDecoration(
                                  labelText: settings['email']
                                      .toString()
                                      .toUpperCase(),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0))),
                            ),
                            Padding(padding: EdgeInsets.only(top: 10)),
                            TextFormField(
                              style: Theme.of(context)
                                  .textTheme
                                  .body1
                                  .merge(TextStyle(height: 1)),
                              controller: nameController,
                              decoration: InputDecoration(
                                  labelText:
                                      settings['name'].toString().toUpperCase(),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0))),
                            ),
                            Padding(padding: EdgeInsets.only(top: 10)),
                            TextFormField(
                              style: Theme.of(context)
                                  .textTheme
                                  .body1
                                  .merge(TextStyle(height: 1)),
                              controller: streetController,
                              decoration: InputDecoration(
                                  labelText: settings['street']
                                      .toString()
                                      .toUpperCase(),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0))),
                            ),
                            Padding(padding: EdgeInsets.only(top: 10)),
                            Row(
                              children: <Widget>[
                                Flexible(
                                  flex: 6,
                                  child: TextFormField(
                                    style: Theme.of(context)
                                        .textTheme
                                        .body1
                                        .merge(TextStyle(height: 1)),
                                    controller: zipController,
                                    decoration: InputDecoration(
                                        labelText: settings['zip']
                                            .toString()
                                            .toUpperCase(),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(0))),
                                  ),
                                ),
                                Spacer(
                                  flex: 1,
                                ),
                                Flexible(
                                  flex: 20,
                                  child: TextFormField(
                                    style: Theme.of(context)
                                        .textTheme
                                        .body1
                                        .merge(TextStyle(height: 1)),
                                    controller: cityController,
                                    decoration: InputDecoration(
                                        labelText: settings['city']
                                            .toString()
                                            .toUpperCase(),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(0))),
                                  ),
                                )
                              ],
                            ),
                            /*
                            Padding(padding: EdgeInsets.only(top: 10)),
                            Row(
                              children: <Widget>[
                                Flexible(
                                  flex: 12,
                                  child: TextFormField(
                                    style: Theme.of(context)
                                        .textTheme
                                        .body1
                                        .merge(TextStyle(height: 1)),
                                    controller: fonController,
                                    decoration: InputDecoration(
                                        labelText: settings['fon']
                                            .toString()
                                            .toUpperCase(),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(0))),
                                  ),
                                ),
                                Spacer(
                                  flex: 1,
                                ),
                                Flexible(
                                  flex: 12,
                                  child: TextFormField(
                                    style: Theme.of(context)
                                        .textTheme
                                        .body1
                                        .merge(TextStyle(height: 1)),
                                    controller: mobileController,
                                    decoration: InputDecoration(
                                        labelText: settings['mobile']
                                            .toString()
                                            .toUpperCase(),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(0))),
                                  ),
                                )
                              ],
                            ),
                            Padding(padding: EdgeInsets.only(top: 10)),
                            TextFormField(
                              style: Theme.of(context)
                                  .textTheme
                                  .body1
                                  .merge(TextStyle(height: 1)),
                              controller: birthdayController,
                              decoration: InputDecoration(
                                  labelText: settings['birthday']
                                      .toString()
                                      .toUpperCase(),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0))),
                            ),
                            Padding(padding: EdgeInsets.only(top: 10)),
                            TextFormField(
                              style: Theme.of(context)
                                  .textTheme
                                  .body1
                                  .merge(TextStyle(height: 1)),
                              controller: intnumberController,
                              decoration: InputDecoration(
                                  labelText: settings['intnumber']
                                      .toString()
                                      .toUpperCase(),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0))),
                            ),
                            Padding(padding: EdgeInsets.only(top: 10)),
                            TextFormField(
                              style: Theme.of(context)
                                  .textTheme
                                  .body1
                                  .merge(TextStyle(height: 1)),
                              controller: ibanController,
                              decoration: InputDecoration(
                                  labelText: settings['iban'],
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0))),
                            ),
                            Padding(padding: EdgeInsets.only(top: 10)),
                            */
                            /*
                            TextFormField(
                              style: Theme.of(context)
                                  .textTheme
                                  .body1
                                  .merge(TextStyle(height: 1)),
                              controller: bicController,
                              decoration: InputDecoration(
                                  labelText:
                                      settings['bic'].toString().toUpperCase(),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0))),

                            Padding(
                              padding: EdgeInsets.only(top: 15),
                            ),
                            */
                            _saveButton(settings, isDark),
                            Padding(
                              padding: EdgeInsets.only(top: 15),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new SwitchListTile(
                                    // 333333
                                    activeColor: switchOn,
                                    activeTrackColor: switchBackground,
                                    inactiveThumbColor: switchOff,
                                    contentPadding: EdgeInsets.all(0),
                                    value: _isPush,
                                    onChanged: (value) {
                                      print(value);
                                      if (value == false) {
                                        _pushUnsubscribe();
                                      } else {
                                        _pushSubscribe();
                                      }
                                    },
                                    title: new Text(settings['push'],
                                        style: Theme.of(context)
                                            .textTheme
                                            .body1
                                            .merge(TextStyle()))),
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.only(bottom: 30),
                            ),
                          ]),
                    )))));
  }

  @override
  Widget build(BuildContext context) {
    dynamic settings = GlobalConfiguration().get("mydata");
    dynamic appSettings = GlobalConfiguration().get("app");
    _queryData = MediaQuery.of(context);

    return BaseWidget(builder: (context, sizingInformation) {
      return Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            /*
            leading: IconButton(
              icon: new Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
             */
            title: Text(settings['title']),
            actions: <Widget>[_btnDeleteMyData(settings)],
          ),
          body: _getForm(settings, appSettings, sizingInformation, _queryData));
    });
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the widget tree.
    // This also removes the _printLatestValue listener.
    emailController.dispose();
    nameController.dispose();
    streetController.dispose();
    zipController.dispose();
    cityController.dispose();
    fonController.dispose();
    mobileController.dispose();
    birthdayController.dispose();
    intnumberController.dispose();
    ibanController.dispose();
    bicController.dispose();
    super.dispose();
  }
}
