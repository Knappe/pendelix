import 'package:get_it/get_it.dart';
import 'package:pendelix/helper/service_navigation.dart';

GetIt locator = GetIt.I;

void setupLocator() {
  locator.registerLazySingleton(() => NavigationService());
}