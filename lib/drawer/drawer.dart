import 'package:flutter/material.dart';
import 'package:pendelix/texts/texts.dart';
import 'package:pendelix/mydata/mydata.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:url_launcher/url_launcher.dart';

enum DrawerSelection {
  home,
  mydata,
  help,
  about,
  contact,
  imprint,
  protect,
  policy
}

class AppDrawer extends StatelessWidget {
  DrawerSelection _drawerSelection = DrawerSelection.home;

  dynamic settings = GlobalConfiguration().get("drawer");

  _launchUrl(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Theme(
        data: Theme.of(context).copyWith(
            canvasColor: Color.fromRGBO(0, 0, 0, 0.8),
            textTheme:
                Theme.of(context).textTheme.apply(bodyColor: Colors.white)),
        child: Drawer(
            child: ListView(
          // Important: Remove any padding from the ListView.
          // padding: EdgeInsets.zero,
          children: ListTile.divideTiles(
            color: Color.fromRGBO(0, 0, 0, 0.0),
            context: context,
            tiles: [
              Container(
                  height: 92,
                  color: Color(0XFF333333),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      ListTile(
                        leading: Icon(Icons.close, color: Colors.white),
                        title: Text('Menu',
                            style: Theme.of(context).textTheme.headline.merge(
                                TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.white))),
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  )),
              ListTile(
                leading: Icon(Icons.person_outline, color: Color(0XFF9e9e9e)),
                title: Text(settings['mydata'],
                    style: Theme.of(context).textTheme.headline),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MyData()),
                  );
                },
              ),
              Padding(padding: EdgeInsets.only(top: 5)),
              ListTile(
                leading: Icon(Icons.help_outline, color: Color(0XFF9e9e9e)),
                title: Text(settings['help'],
                    style: Theme.of(context).textTheme.headline),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => TextHelp()),
                  );
                },
              ),
              Padding(padding: EdgeInsets.only(top: 5)),
              ListTile(
                leading: Icon(Icons.info_outline, color: Color(0XFF9e9e9e)),
                title: Text(settings['about'],
                    style: Theme.of(context).textTheme.headline),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => TextAbout()),
                  );
                },
              ),
              Padding(padding: EdgeInsets.only(top: 5)),
              ListTile(
                leading: Icon(Icons.question_answer, color: Color(0XFF9e9e9e)),
                title: Text(settings['contact'],
                    style: Theme.of(context).textTheme.headline),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => TextContact()),
                  );
                },
              ),
              Padding(padding: EdgeInsets.only(top: 5)),
              ListTile(
                leading: Icon(Icons.list, color: Color(0XFF9e9e9e)),
                title: Text(settings['imprint'],
                    style: Theme.of(context).textTheme.headline),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => TextImprint()),
                  );
                },
              ),
              Padding(padding: EdgeInsets.only(top: 5)),
              ListTile(
                leading: Icon(Icons.security, color: Color(0XFF9e9e9e)),
                title: Text(settings['dataprotection'],
                    style: Theme.of(context).textTheme.headline),
                onTap: () {
                  Navigator.of(context).pop();
                  _launchUrl(settings['dataprotectionlink']);
                },
              ),
              Padding(padding: EdgeInsets.only(top: 5)),
              ListTile(
                leading: Icon(Icons.check, color: Color(0XFF9e9e9e)),
                title: Text(settings['policy'],
                    style: Theme.of(context).textTheme.headline),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => TextPolicy()),
                  );
                },
              ),
            ],
          ).toList(),
        )));
  }
}
