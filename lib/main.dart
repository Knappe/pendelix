import 'dart:async';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:pendelix/home/home.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'package:pendelix/firebase/push.dart';
import 'package:pendelix/helper/service_locator.dart';
import 'package:pendelix/helper/service_navigation.dart';
import 'package:pendelix/article/article.dart';
import 'package:pendelix/article/article_deep.dart';

Future<void> _loadConfigFromPath() async {
  String _dir = (await getApplicationDocumentsDirectory()).path;
  await GlobalConfiguration().loadFromPath("$_dir/_settings.json");
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  // await GlobalConfiguration().loadFromPath("_settings.json");
  // _loadConfigFromPath();
  await GlobalConfiguration().loadFromAsset("settings");
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  PushNotificationsManager().init();
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  dynamic settings = GlobalConfiguration().get("main");

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      navigatorKey: locator<NavigationService>().navigatorKey,
      title: settings['title'],
      theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          brightness: Brightness.light,
          primaryColor: Colors.black,
          accentColor: Colors.white,
          fontFamily: settings['fontfamily'],
          dividerColor: Color(0XFFc8c8c8),
          buttonTheme: ButtonThemeData(
            splashColor: Color(0XFFd6d6d6),
            buttonColor: Color(0XFFd6d6d6),
            shape: BeveledRectangleBorder(
              borderRadius: BorderRadius.circular(0),
            ),
          ),
          appBarTheme: AppBarTheme(color: Color(0XFF333333)),
          tabBarTheme: TabBarTheme(
              labelStyle: TextStyle(
                  fontFamily: 'clan-news',
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1.25),
              labelColor: Colors.white,
              unselectedLabelColor: Colors.grey),
          textTheme: TextTheme(
              title: TextStyle(fontFamily: 'clan-bold', height: 1.32),
              subtitle: TextStyle(fontFamily: 'clan-news', fontSize: 17),
              subhead: TextStyle(
                  fontFamily: 'clan-medium',
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                  letterSpacing: 1.25),
              overline: TextStyle(fontFamily: 'clan-news', fontSize: 12),
              headline: TextStyle(
                  fontFamily: 'clan-medium',
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                  color: Color(0XFF9e9e9e),
              ),
              body1: TextStyle(
                  fontFamily: 'clan-news', height: 1.57, fontSize: 14),
              body2: TextStyle(height: settings['deflineheight']),
              display4: TextStyle(height: settings['deflineheight']),
              display3: TextStyle(height: settings['deflineheight']),
              display2: TextStyle(height: settings['deflineheight']),
              display1: TextStyle(height: settings['deflineheight']),
              caption: TextStyle(height: settings['deflineheight']),
              button: TextStyle(height: settings['deflineheight']))),
      darkTheme: ThemeData(
          scaffoldBackgroundColor: Color(0XFF121212),
          brightness: Brightness.dark,
          accentColor: Colors.white,
          fontFamily: settings['fontfamily'],
          dividerColor: Color(0XFF333333),
          buttonTheme: ButtonThemeData(
            splashColor: Color(0XFF333333),
            buttonColor: Color(0XFF333333),
            shape: BeveledRectangleBorder(
              borderRadius: BorderRadius.circular(0),
            ),
          ),
          appBarTheme: AppBarTheme(color: Color(0XFF333333)),
          tabBarTheme: TabBarTheme(
              labelStyle: TextStyle(
                  fontFamily: 'clan-news',
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1.25),
              labelColor: Colors.white,
              unselectedLabelColor: Colors.grey),
          textTheme: TextTheme(
              title: TextStyle(fontFamily: 'clan-bold', height: 1.32),
              subtitle: TextStyle(fontFamily: 'clan-news', fontSize: 17),
              subhead: TextStyle(
                  fontFamily: 'clan-medium',
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  color: Colors.black,
                  letterSpacing: 1.25),
              overline: TextStyle(fontFamily: 'clan-news', fontSize: 12),
              headline: TextStyle(
                fontFamily: 'clan-medium',
                fontSize: 15,
                fontWeight: FontWeight.w500,
                color: Color(0XFF9e9e9e),
              ),
              body1: TextStyle(
                  fontFamily: 'clan-news', height: 1.57, fontSize: 14),
              body2: TextStyle(height: settings['deflineheight']),
              display4: TextStyle(height: settings['deflineheight']),
              display3: TextStyle(height: settings['deflineheight']),
              display2: TextStyle(height: settings['deflineheight']),
              display1: TextStyle(height: settings['deflineheight']),
              caption: TextStyle(height: settings['deflineheight']),
              button: TextStyle(height: settings['deflineheight']))),
      home: Home(),
      onGenerateRoute: generateRoute,
      /*
      routes: {
        '/article': (context) => Article(),
        '/articledeep': (context) => ArticleDeep()
      },
       */
    );
  }

  Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/articledeep':
        return MaterialPageRoute(
            builder: (context) => ArticleDeep(id: settings.arguments));
    }
  }
}

class DrawerStateInfo with ChangeNotifier {
  int _currentDrawer = 0;
  int get getCurrentDrawer => _currentDrawer;

  void setCurrentDrawer(int drawer) {
    _currentDrawer = drawer;
    notifyListeners();
  }

  void increment() {
    notifyListeners();
  }
}