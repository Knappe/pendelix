import 'package:flutter/material.dart';
import 'package:pendelix/drawer/drawer.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:pendelix/database/database.dart';
import 'package:pendelix/article/article.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import 'package:pendelix/mydata/mydata.dart';
import 'dart:convert';
import 'dart:io';
import 'package:pendelix/ui/base_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:pendelix/texts/texts.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:pendelix/home/popup.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pendelix/form/report/report_list.dart';
import 'package:pendelix/track/track_list.dart';


class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  TabController _tabController;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var _db;
  String _co = 'nix';
  String _dir;
  MediaQueryData _queryData;

  void initState() {
    dynamic settings = GlobalConfiguration().get("main");
    // dynamic down = _downloadConfig(settings);
    // _db = new DatabaseHelper();
    _dbInit();
  }

  void _dbInit() async {
    _db = new DatabaseHelper();
    await _db.getDb();
  }

  _getWidth(sizingInformation, appSettings) {
    if (sizingInformation.screenSize.width > appSettings['breakpoint']) {
      return appSettings['breakpoint'];
    } else {
      return sizingInformation.screenSize.width;
    }
  }

  _navigateRoute(route){
    if(route == null){
      return false;
    }
    switch(route){
      case "reportform":
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ReportList()),
        );
        break;
      case "mydata":
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => MyData()),
        );
        break;
      case "track":
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => TrackList()),
        );
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    dynamic settings = GlobalConfiguration().get("main");
    dynamic appSettings = GlobalConfiguration().get("app");
    _queryData = MediaQuery.of(context);

    Items item1 = new Items(
        title: "Melden",
        subtitle: "Falschparker, Schlaglöcher...",
        event: "",
        icon: Icons.report,
        route: "reportform"
        );

    Items item2 = new Items(
      title: "Meine Daten",
      subtitle: "Bei zukünftiger Verwendung der App..",
      event: "2 Items",
        icon: Icons.person_outline,
        route: "mydata"
    );

    Items item3 = new Items(
        title: "Routen",
        subtitle: "Die schönsten Penlderrouten..",
        event: "2 Items",
        icon: Icons.directions_bike,
        route: "track"
    );

    List<Items> myList = [item1, item2, item3];
    var color = 0xff6b6b6b;

    return BaseWidget(builder: (context, sizingInformation) {
      return Scaffold(
          key: _scaffoldKey,
          drawer: Drawer(
            child: new AppDrawer(),
          ),
          appBar: AppBar(
              title:
              Padding(
                padding: EdgeInsets.only(bottom: 10),
          child:
              Row(
                children: <Widget>[
                  Image.asset('assets/img/logo.png',
                      fit: BoxFit.contain, height: 30),
                ],
              ),
          )),
          body:
          Column(
              children: <Widget>[
          Flexible(
            child: GridView.count(
                childAspectRatio: 1.0,
                padding: EdgeInsets.only(top: 18, left: 14, right: 14),
                crossAxisCount: 2,
                crossAxisSpacing: 16,
                mainAxisSpacing: 16,
                children: myList.map((data) {
                  return GestureDetector(
                    onTap: (){
                        if(data.route != null){
                          _navigateRoute(data.route);
                        }
                    },
                  child:
                  Container(
                    decoration: BoxDecoration(
                        color: Color(color), borderRadius: BorderRadius.circular(5)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: 8,
                        ),
                        Icon(
                          data.icon,
                          color: Colors.white,
                          size: 30.0,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          data.title,
                          textAlign: TextAlign.center,
                          style: GoogleFonts.openSans(
                              textStyle: TextStyle(
                                  color: Colors.white,
                                  fontSize: 22,
                                  fontWeight: FontWeight.w600)),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Padding(
                          padding: EdgeInsets.only(left:10, right:10),
                        child:
                        Text(
                          data.subtitle,
                          textAlign: TextAlign.center,
                          style: GoogleFonts.openSans(
                              textStyle: TextStyle(
                                  color: Colors.white54,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600)),
                        )),
                        SizedBox(
                          height: 8,
                        ),
                      ],
                    ),
                  ));
                }).toList()),
          )])
      );
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
}

class Items {
  String title;
  String subtitle;
  String event;
  IconData icon;
  String route;
  Items({this.title, this.subtitle, this.event, this.icon, this.route});
}
