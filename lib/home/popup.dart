import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';

class PopUp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var brightness = MediaQuery.of(context).platformBrightness;
    // print(brightness);
    dynamic settings = GlobalConfiguration().get("main");
    return Dialog(
        shape: BeveledRectangleBorder(
          borderRadius: BorderRadius.circular(0),
        ),
        elevation: 0.0,
        child: GestureDetector(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: IntrinsicWidth(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Flexible(
                    child: SingleChildScrollView(
                      child: Stack(children: <Widget>[
                        Positioned(
                          right: 10.0,
                          top: 10.0,
                          child: Align(
                            alignment: Alignment.topRight,
                            child: CircleAvatar(
                              radius: 14.0,
                              backgroundColor: Color(0XFF333333),
                              child: Icon(Icons.close, color: Colors.white),
                            ),
                          ),
                        ),
                        Padding(
                            padding: EdgeInsets.all(20),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Image.asset('assets/img/logo-neu.png',
                                        fit: BoxFit.contain, height: 35),
                                    Padding(
                                      padding: EdgeInsets.only(left: 5),
                                    ),
                                    Text(settings['popup']['ictitle'],
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle
                                            .merge(TextStyle())),
                                  ],
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 10),
                                ),
                                Text(
                                  settings['popup']['title'],
                                  style: Theme.of(context)
                                      .textTheme
                                      .title
                                      .merge(TextStyle()),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 10),
                                ),
                                Text(settings['popup']['text'],
                                    style: Theme.of(context)
                                        .textTheme
                                        .body1
                                        .merge(TextStyle())),
                              ],
                            )),
                      ]),
                    ),
                  )
                ],
              ),
            )));
  }
}
